import sys
from os import path
MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", 'api_backend', 'app'))
sys.path.append(MOD_COMMON_PATH)

from common.utils import Stopwatch
from time import sleep
from celery import Celery, signature, group

app = Celery()
app.config_from_object('celeryconfig')

# from celery.task.control import inspect
# i = inspect()
# print(i.registered_tasks())

def run_celery_task_by_name(name, kwargs):
    task = signature(f'app.main_api_backend.{name}', kwargs=kwargs)
    with Stopwatch()(f'Fetching data from the worker'):
        return task.apply_async().get()


# _internal_fetch_additional_data(self, lon, lat, description):
def test___internal_fetch_additional_data():
    kwargs = {
        "lon": 14.42076,
        "lat": 50.08804,
        "description": ''
    }
    ret = run_celery_task_by_name('_internal_fetch_additional_data', kwargs)
    print(ret)

# get_flat_price_estimator_all_features(self, with_details):
def test__get_flat_price_estimator_all_features():
    kwargs = {
        'with_details': False
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_all_features', kwargs)

    kwargs = {
        'with_details': True
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_all_features', kwargs)


# get_flat_price_estimator_api_features(self, with_details):
def test__get_flat_price_estimator_api_features():
    kwargs = {
        'with_details': False
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_api_features', kwargs)

    kwargs = {
        'with_details': True
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_api_features', kwargs)


# get_flat_price_estimator_all_features_loss_gain(self):
def test__get_flat_price_estimator_all_features_loss_gain():
    kwargs = {}
    ret = run_celery_task_by_name('get_flat_price_estimator_all_features_loss_gain', kwargs)


# get_flat_price_estimator_api_features_loss_gain(self):
def test__get_flat_price_estimator_api_features_loss_gain():
    kwargs = {}
    ret = run_celery_task_by_name('get_flat_price_estimator_api_features_loss_gain', kwargs)


# get_flat_price_estimator_testing_error_statistics(self):
def test__get_flat_price_estimator_testing_error_statistics():
    kwargs = {}
    ret = run_celery_task_by_name('get_flat_price_estimator_testing_error_statistics', kwargs)


# get_flat_surroundings_types(self):
def test__get_flat_surroundings_types():
    kwargs = {}
    ret = run_celery_task_by_name('get_flat_surroundings_types', kwargs)


# def get_flat_surroundings(self, lon, lat, max_distance):
def test__get_flat_surroundings():
    kwargs = {
        "lon": 14.42076,
        "lat": 50.08804,
        "max_distance": 500
    }
    ret = run_celery_task_by_name('get_flat_surroundings', kwargs)


# def get_area_flood_risk(self, lon, lat, max_distance):
def test__get_area_flood_risk():
    kwargs = {
        "lon": 14.42076,
        "lat": 50.08804
    }
    ret = run_celery_task_by_name('get_area_flood_risk', kwargs)


# def get_flat_price_estimator_estimation(self, lon, lat, description, external_api_data, with_error_estimation=False):
def test__get_flat_price_estimator_estimation():
    external_api_data = {
        "sreal_balkon_nom_boolean": True,
        "sreal_bazen_nom_boolean": True,
        "sreal_bezbarierovy_nom_boolean": True,
        "sreal_doprava_autobus_nom_boolean": True,
        "sreal_doprava_dalnice_nom_boolean": True,
        "sreal_doprava_mhd_nom_boolean": True,
        "sreal_doprava_silnice_nom_boolean": True,
        "sreal_doprava_vlak_nom_boolean": True,
        "sreal_elektrina_120v_nom_boolean": True,
        "sreal_elektrina_400v_nom_boolean": True,
        "sreal_garaz_nom_boolean": True,
        "sreal_komunikace_asfaltova_nom_boolean": True,
        "sreal_komunikace_betonova_nom_boolean": True,
        "sreal_komunikace_dlazdena_nom_boolean": True,
        "sreal_komunikace_neupravena_nom_boolean": True,
        "sreal_lodzie_nom_boolean": True,
        "sreal_odpad_cov_pro_cely_objekt_nom_boolean": True,
        "sreal_odpad_jimka_nom_boolean": True,
        "sreal_odpad_septik_nom_boolean": True,
        "sreal_odpad_verejna_kanalizace_nom_boolean": True,
        "sreal_parkovani_nom_boolean": True,
        "sreal_plyn_individualni_nom_boolean": True,
        "sreal_pudni_vestavba_nom_boolean": True,
        "sreal_sklep_nom_boolean": True,
        "sreal_telekomunikace_kabelova_televize_nom_boolean": True,
        "sreal_telekomunikace_kabelove_rozvody_nom_boolean": True,
        "sreal_telekomunikace_ostatni_nom_boolean": True,
        "sreal_telekomunikace_satelit_nom_boolean": True,
        "sreal_terasa_nom_boolean": True,
        "sreal_topeni_jine_nom_boolean": True,
        "sreal_topeni_lokalni_elektricke_nom_boolean": True,
        "sreal_topeni_lokalni_plynove_nom_boolean": True,
        "sreal_topeni_lokalni_tuha_paliva_nom_boolean": True,
        "sreal_topeni_ustredni_dalkove_nom_boolean": True,
        "sreal_topeni_ustredni_elektricke_nom_boolean": True,
        "sreal_topeni_ustredni_plynove_nom_boolean": True,
        "sreal_topeni_ustredni_tuha_paliva_nom_boolean": True,
        "sreal_voda_mistni_zdroj_nom_boolean": True,
        "sreal_vybaveni_nom_boolean": True,
        'sreal_balkon_ord_float': 100,
        'sreal_elektrina_230v_nom_boolean': True,
        'sreal_garaz_ord_integer': 75,
        'sreal_lodzie_ord_float': 100,
        'sreal_parkovani_ord_integer': 75,
        'sreal_plocha_podlahova_ord_float': 100,
        'sreal_plocha_zahrady_ord_float': 100,
        'sreal_plocha_zastavena_ord_float': 100,
        'sreal_plyn_plynovod_nom_boolean': True,
        'sreal_pocet_bytu_ord_integer': 75,
        'sreal_prevod_do_ov_nom_string': 'ano',
        'sreal_rok_kolaudace_ord_integer': 75,
        'sreal_sklep_ord_float': 100,
        'sreal_stav_nom_string': 'rezervovano',
        'sreal_stav_objektu_nom_string': 'dobry',
        'sreal_stavba_nom_string': 'kamenna',
        'sreal_telekomunikace_internet_nom_boolean': True,
        'sreal_telekomunikace_telefon_nom_boolean': True,
        'sreal_terasa_ord_float': 100,
        'sreal_umisteni_objektu_nom_string': 'klidna_cast_obce',
        'sreal_uzitna_plocha_ord_float': 100,
        'sreal_vlastnictvi_nom_string': 'osobni',
        'sreal_voda_dalkovy_vodovod_nom_boolean': True,
        'sreal_vybaveni_nom_string': 'castecne',
        'sreal_vyska_stropu_ord_float': 100,
        'sreal_vytah_nom_boolean': True

    }

    kwargs = {
        "lon": 14.42076,
        "lat": 50.08804,
        "description": '',
        "external_api_data": external_api_data,
        "with_error_estimation": True
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_estimation', kwargs)
    
if __name__ == '__main__':
    sw = Stopwatch()

    with sw('Runing all tests'):
        # TODO build tests around this!
        test___internal_fetch_additional_data()
        # test__get_flat_price_estimator_all_features()
        # test__get_flat_price_estimator_api_features()
        # test__get_flat_price_estimator_all_features_loss_gain()
        # test__get_flat_price_estimator_api_features_loss_gain()
        # test__get_flat_price_estimator_testing_error_statistics()
        # test__get_flat_surroundings_types()
        # test__get_flat_surroundings()
        # test__get_area_flood_risk()
        # test__get_flat_price_estimator_estimation()
