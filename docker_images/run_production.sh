#!/bin/bash

docker-compose -f docker-compose-production.yaml up -d rabbit
docker-compose -f docker-compose-production.yaml up -d redis
docker-compose -f docker-compose-production.yaml up -d data_cso
docker-compose -f docker-compose-production.yaml up -d data_surroundings_and_floods
docker-compose -f docker-compose-production.yaml up -d data_reverse_geocoder
# docker-compose -f docker-compose-production.yaml up -d api_backend
# docker-compose -f docker-compose-production.yaml up -d api_frontend
