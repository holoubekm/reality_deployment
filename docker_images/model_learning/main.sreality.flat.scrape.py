from common.utils import Stopwatch
from scrapers.sreality.flat.stubs_scraper import SrealityStubsScraper
from scrapers.sreality.flat.details_scraper import SrealityDetailsScraper
from scrapers.sreality.flat.features_miner import SrealityFeaturesMiner
# from scrapers.sreality.flat.images_scraper import SrealityImagesScraper

if __name__ == '__main__':

    sw = Stopwatch()
    # with sw('Scraping new stems from [sreality.cz] in a delta way'):
    #      stubs_scraper = SrealityStubsScraper.get_instance()
    #      stubs_scraper.scrape(per_page=250, start_page=1, max_pages=999999)
    #      del stubs_scraper
    
    # with sw('Scraping details for previously saved stems from [sreality.cz]'):
    #     details_scraper = SrealityDetailsScraper.get_instance()
    #     details_scraper.scrape()
    #     del details_scraper
    
    with sw('Mining features from the raw scraped data'):
        features_miner = SrealityFeaturesMiner.get_instance()
        features_miner.mine()
        del features_miner

    # with sw('Scraping all images'):
    #     images_scraper = SrealityImagesScraper.get_instance()
    #     images_scraper.scrape()
    #     del images_scraper

