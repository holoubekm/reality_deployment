from common.utils import text_lower_rm_accents_rm_ws, parse_int, parse_float
from scrapers.scraper import SrealityScraper
from datetime import datetime

from queue  import Queue, Empty

import threading
import requests
from pymongo import TEXT


class SrealityImagesScraper(SrealityScraper):
    __instance__ = None
    def __init__(self) -> None:
        super().__init__()
        self.images_collection_name = f'sreality.cz_flat_images'
        self.features_collection_name = f'sreality.cz_flat_features'

    def get_instance():
        if SrealityImagesScraper.__instance__ == None:
            SrealityImagesScraper.__instance__ = SrealityImagesScraper()
        return SrealityImagesScraper.__instance__

    @staticmethod
    def save_result(collection, result):
        is_duplicate = {"filename": result['filename']}
        written = collection.update_one(is_duplicate, {"$setOnInsert": result}, upsert=True)
        if written.upserted_id:
            print(f'Mined features for reality with an id: [{written.upserted_id}]')

    def scrape(self):
        with self.get_connection() as connection:
            db = connection[self.database_name]
            features_collection = db[self.features_collection_name]
            self.create_mongo_collection_if_not_exists(
                db,
                self.images_collection_name,
                lambda collection:
                    collection.createIndex([('filename', pymongo.ASCENDING)])
            )
            images_collection = db[self.images_collection_name]

            features = features_collection.find()

            imglinksqueue = Queue()


            print("Generating images to scrape")
            for item in features:
                #scrape all images for each f
                for image in item["images"]:
                    imglinksqueue.put(image);

            class Parser(threading.Thread):

                thread_id = None

                def __init__(self_thread, id):
                    threading.Thread.__init__(self_thread)
                    self_thread.thread_id = id

                def process_item(self_thread, image):
                    if images_collection.find_one({'filename': image}) is None:
                        print(f'Downloading (left {imglinksqueue.qsize()}) - {image}')
                        try:
                            imgdata = self.make_get_request(image).content
                            fileID = self.save_file(db, imgdata, filename=image)

                            result = {
                                "filename": image,
                                "fileID": fileID
                            }

                            self.save_result(images_collection, result)

                            print("...image successfully downloaded")
                        except requests.exceptions.RequestException as e:  # This is the correct syntax
                            print(" ... FAILED duet to RequestException")
                    else:
                        print(f'...image already exists (left {imglinksqueue.qsize()})')

                def run(self_thread):
                    print(f'Start of scraper {self_thread.thread_id}')
                    try:
                        while True:
                            queue_ret = imglinksqueue.get(block=False)
                            self_thread.process_item(queue_ret)
                            print("Retrieved", queue_ret)
                    except Empty:
                        pass

            SCRAPERS = 20

            print(f'Initializing {SCRAPERS} scrapers')
            parsers = []
            for i in range(0,20):
                parsers.append(Parser(i))


            for parser in parsers:
                parser.start()

            for parser in parsers:
                parser.strart()