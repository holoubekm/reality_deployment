
from uuid import uuid4
from json import loads as json_loads
from time import sleep

from scrapers.scraper import SrealityScraper

class SrealityDetailsScraper(SrealityScraper):
    __instance__ = None

    def __init__(self):
        super().__init__()
        self.stubs_collection_name = f'sreality.cz_flat_stubs'
        self.details_collection_name = f'sreality.cz_flat_details'

    def get_instance():
        if SrealityDetailsScraper.__instance__ == None:
            SrealityDetailsScraper.__instance__ = SrealityDetailsScraper()
        return SrealityDetailsScraper.__instance__

    def get_existing_hash_ids(self):
        with self.get_connection() as connection:
            database = connection["scraped"]
            if self.stubs_collection_name not in database.list_collection_names():
                raise InternalException('Please set value of [self.stubs_collection_name]')

            print(f'Loading data from [{self.stubs_collection_name}]')
            collection = database[self.stubs_collection_name]
            results = collection.find({}, {'hash_id': 1})
            return [result['hash_id'] for result in results]

    def scrape(self):
        hash_ids = self.get_existing_hash_ids()
        print(f'Number of estates in database: {len(hash_ids):05d}')
        
        with self.get_connection() as connection:
            database = connection["scraped"]
            self.create_mongo_collection_if_not_exists(database, self.details_collection_name)
            collection = database[self.details_collection_name]

            print(f'Saving data to [{self.details_collection_name}]')

            written_count = 0
            for hash_id in hash_ids:
                found = collection.find({'hash_id': hash_id}).count()
                if not found:
                    url = f'https://www.sreality.cz/api/cs/v2/estates/{hash_id}'
                    data = self.make_get_request(url).json()
                    item = {
                        "id": str(uuid4()),
                        'inserted_at': self.date_time_formatted(),
                        'data': data,
                        'hash_id': hash_id
                    }

                    is_duplicate = {"hash_id": hash_id}
                    written = collection.update_one(is_duplicate, {"$setOnInsert": item}, upsert=True)
                    if written.upserted_id:
                        print(f'Saved hash_id: [{hash_id}]')
                        written_count += 1
                else:
                    print(f'Skipping hash_id: [{hash_id}] - already exists')
            print(f'Written: [{written_count:03d}] / [{len(hash_ids):03d}] estates in total')
