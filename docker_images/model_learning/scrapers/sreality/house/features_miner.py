from common.utils import text_lower_rm_accents_rm_ws, parse_int, parse_float
from scrapers.scraper import SrealityScraper
from datetime import datetime

class SrealityFeaturesMiner(SrealityScraper):
    __instance__ = None
    def __init__(self) -> None:
        super().__init__()
        self.details_collection_name = f'sreality.cz_house_details'
        self.features_collection_name = f'sreality.cz_house_features'
        self.excluded_names = [
            "aktualizace",
            "datum_nastehovani",
            # "energeticka_narocnost_budovy",
            "id_zakazky",
            "poznamka_k_cene",
            # "rok_rekonstrukce",
            "datum_ukonceni_vystavby",
            "anuita",
            # "podlazi",
            "naklady_na_bydleni",
            "minimalni_kupni_cena",
            "pocet_kancelari",
            # "plocha_bazenu",
            "id",
            "zlevneno",
            "cena",
            "provize",
            "datum_zahajeni_prodeje",
            "datum_prohlidky",
            "datum_prohlidky_do",
            "puvodni_cena",
            "prukaz_energeticke_narocnosti_budovy",
            "ukazatel_energeticke_narocnosti_budovy"
          ]

        self.translation_table = {
             2: '1+kk',
             3: '1+1',
             4: '2+kk',
             5: '2+1',
             6: '3+kk',
             7: '3+1',
             8: '4+kk',
             9: '4+1',
            10: '5+kk',
            11: '5+1',
            12: '6-a-vice',
            16: 'atypicky',
            18: 'komercni-pozemky',
            19: 'stavebni-parcely',
            20: 'pole',
            21: 'lesy',
            22: 'louky',
            23: 'zahrady',
            24: 'ostatni-pozemky',
            25: 'kancelare',
            26: 'sklady',
            27: 'vyrobni-prostory',
            28: 'obchodni-prostory',
            29: 'ubytovani',
            30: 'restaurace',
            31: 'zemedelske-objekty',
            32: 'ostatni-komercni-prostory',
            33: 'chaty',
            34: 'garaze',
            35: 'pamatky-jine',
            36: 'jine-nemovitosti',
            37: 'rodinne-domy',
            38: 'cinzovni-domy',
            39: 'vily',
            40: 'projekty-na-klic',
            43: 'chalupy',
            44: 'zemedelske-usedlosti',
            46: 'rybniky',
            47: 'pokoj',
            48: 'sady-vinice',
            49: 'virtualni-kancelare',
            50: 'vinne-sklepy',
            51: 'pudni-prostory',
            52: 'garazova-stani',
            53: 'mobilheimy'
        }

    def get_instance():
        if SrealityFeaturesMiner.__instance__ == None:
            SrealityFeaturesMiner.__instance__ = SrealityFeaturesMiner()
        return SrealityFeaturesMiner.__instance__

    def item_data_to_features(self, inputs):
        data = {}
        price = None

        if 'items' in inputs:
            for trait in inputs['items']:
                t_type = trait['type']
                t_name = trait['name']
                t_value = trait['value']
                t_name_norm = text_lower_rm_accents_rm_ws(t_name)

                if t_name_norm in self.excluded_names:
                    continue

                if t_name_norm in ['pocet_bytu', 'pocet_kancelari']:
                    key = f'sreal_{t_name_norm}_ord_integer'
                    data[key] = parse_int(t_value)
                    continue

                if t_name_norm == 'vyska_stropu':
                    key = f'sreal_{t_name_norm}_ord_float'
                    val = parse_float(t_value)
                    while val < 1:
                        val = val * 10
                        
                    while val >= 10:
                        val = val / 10
                        
                    data[key] = val
                    continue

                if t_name_norm in ['rok_rekonstrukce', 'rok_kolaudace']:
                    key = f'sreal_{t_name_norm}_ord_float'
                    data[key] = datetime.now().year - parse_int(t_value)
                    continue

                if t_type == 'area' or t_type == 'length':
                    value = parse_float(t_value)
                    if value:
                        key = f'sreal_{t_name_norm}_ord_float'
                        data[key] = value
                elif t_type == 'boolean':
                    key = f'sreal_{t_name_norm}_nom_boolean'
                    data[key] = text_lower_rm_accents_rm_ws(str(t_value))
                elif t_type == 'count' or t_type == 'integer':
                    value = parse_int(t_value)
                    if value:
                        key = f'sreal_{t_name_norm}_ord_integer'
                        data[key] = value
                elif t_type == 'energy_efficiency_rating':
                    key = f'sreal_{t_name_norm}_nom_string'
                    data[key] = text_lower_rm_accents_rm_ws(t_value)
                elif t_type == 'set':
                    for value in t_value:
                        value_norm = text_lower_rm_accents_rm_ws(value['value'])
                        key = f'sreal_{t_name_norm}_{value_norm}_nom_boolean'
                        data[key] = 'true'
                elif t_type == 'string':
                    key = f'sreal_{t_name_norm}_nom_string'
                    data[key] = text_lower_rm_accents_rm_ws(t_value)
                elif t_type == 'price_czk':
                    if t_name_norm == 'celkova_cena':
                        price = parse_float(t_value)
                    else:
                        raise Exception(f'Unknown price type: [{t_type}]: [{t_name_norm}]')
                else:
                    raise Exception(f'Unknown data type: [{t_type}]: [{t_name_norm}]')

        if 'seo' in inputs:
            category_sub_cb = inputs['seo']['category_sub_cb']
            t_value = self.translation_table[category_sub_cb]
            key = f'sreal_typ_nemovitosti_nom_string'
            data[key] = text_lower_rm_accents_rm_ws(t_value)
            
        return data, price

    def mine_features(self, item):
        item_data = item['data']
        estate_id = item['_id']
        images = []
        # TODO add exception handling here
        if '_embedded' in item_data:
            embedded = item_data['_embedded']
            if 'images' in embedded:
                for img in embedded['images']:
                    if '_links' in img:
                        links = img['_links']
                        if 'view' in links:
                            view = links['view']
                            if 'href' in view:
                                images.append(view['href'])

        coords = {}     
        if "map" in item_data:
            coord = item_data['map']
            if 'lon' in coord and 'lat' in coord:
                coords = {
                    'lon': coord['lon'],
                    'lat': coord['lat']
                }

        # TODO add exception handling here
        description = None
        if 'text' in item_data:
            description_holder = item_data['text']
            if description_holder['name'] == 'Popis':
                description = description_holder['value']

        data, price = self.item_data_to_features(item_data)

        result = {
                    'data': data,
                    'price': price,
                    'description': description,
                    'estate_id': estate_id,
                    'coords': coords,
                    'images': images
                }
        return result

    @staticmethod
    def save_result(collection, result):
        is_duplicate = {"estate_id": result['estate_id']}
        written = collection.update_one(is_duplicate, {"$setOnInsert": result}, upsert=True)
        if written.upserted_id:
            print(f'Mined features for estate with an id: [{written.upserted_id}]')

    def mine(self):
        with self.get_connection() as connection:
            db = connection[self.database_name]
            details_collection = db[self.details_collection_name]
            self.create_mongo_collection_if_not_exists(db, self.features_collection_name)
            features_collection = db[self.features_collection_name]

            details = details_collection.find()
            for item in details:
                result = self.mine_features(item)
                self.save_result(features_collection, result)
