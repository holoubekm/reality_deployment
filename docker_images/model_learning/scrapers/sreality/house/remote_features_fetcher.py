#!/usr/bin/python3
import requests
from uuid import uuid4
from json import loads as json_loads
from time import sleep
from celery import Celery, signature, group

from common.utils import Stopwatch
from scrapers.scraper import SrealityScraper

app = Celery()
app.config_from_object('celeryconfig')

# from celery.task.control import inspect
# i = inspect()
# print(i.registered_tasks())

class SrealityRemoteFeaturesFetcher(SrealityScraper):

    __instance__ = None

    def __init__(self) -> None:
        super().__init__()
        self.features_collection_name = f'sreality.cz_house_features'
        self.fetched_collection_name = f'sreality.cz_house_fetched'

    def get_instance():
        if SrealityRemoteFeaturesFetcher.__instance__ == None:
            SrealityRemoteFeaturesFetcher.__instance__ = SrealityRemoteFeaturesFetcher()
        return SrealityRemoteFeaturesFetcher.__instance__

    def get_existing_items(self):
        with self.get_connection() as connection:
            database = connection[self.database_name]
            if self.features_collection_name not in database.list_collection_names():
                raise InternalException('Please set value of [self.features_collection_name]')

            existing_collection = database[self.fetched_collection_name]
            results = existing_collection.find({}, {'estate_id': 1})
            already_fetched = {item['estate_id'] for item in results}
            
            collection = database[self.features_collection_name]
            results = collection.find({}, {'estate_id': 1})
            all_items = {item['estate_id'] for item in results}
            
            to_do = list(all_items - already_fetched)

            print(f'Loading data from [{self.features_collection_name}]')
            results = collection.find({'estate_id': {'$in': to_do}});
            items = []
            for result in results:
                if 'coords' in result:
                    coords = result['coords']
                    if 'lon' in coords and 'lat' in coords:
                        items.append(result)
            return items

    def save_result(self, estate_id, result):
        with self.get_connection() as connection:
            database = connection[self.database_name]
            fetched_collection = database[self.fetched_collection_name]
            written = fetched_collection.update_one({'estate_id': estate_id}, {"$setOnInsert": result}, upsert=True)
            if written.upserted_id:
                print(f'Fetched features for reality with an id: [{written.upserted_id}]')

    # def get_celery_worker_status(self):
    #     ERROR_KEY = "ERROR"
    #     try:
    #         from celery.task.control import inspect
    #         insp = inspect()
    #         d = insp.stats()
    #         if not d:
    #             d = { ERROR_KEY: 'No running Celery workers were found.' }
    #     except IOError as e:
    #         from errno import errorcode
    #         msg = "Error connecting to the backend: " + str(e)
    #         if len(e.args) > 0 and errorcode.get(e.args[0]) == 'ECONNREFUSED':
    #             msg += ' Check that the RabbitMQ server is running.'
    #         d = { ERROR_KEY: msg }
    #     except ImportError as e:
    #         d = { ERROR_KEY: str(e)}
    #     return d

    def fetch(self):
        batch_size = 10
        
        with self.get_connection() as connection:
            db = connection[self.database_name]
            self.create_mongo_collection_if_not_exists(db, self.fetched_collection_name)

        sw = Stopwatch()
        items = self.get_existing_items()
        batch_index = 0
        while batch_index < len(items):
            batch = items[batch_index:batch_index+batch_size]
            batch_index += batch_size

            metadatas = []
            tasks = []
            for task in batch:
                kwargs = {
                        'lon': task['coords']['lon'], 
                        'lat': task['coords']['lat'],
                        'description': task['description']
                        }
                tasks.append(signature('app.main_api_backend._internal_fetch_additional_data', kwargs=kwargs))
            job = group(tasks)
            with sw(f'Fetching data for another batch of [{batch_size}] items'):
                awaitable = job.apply_async()
                results = awaitable.join()
                
            for task, remote_features in zip(batch, results):
                task['data'].update(remote_features['result'])
                self.save_result(task['estate_id'], task)
