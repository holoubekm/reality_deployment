
from pymongo import MongoClient
from math import ceil
from time import sleep
from datetime import datetime

from common.utils import date_time_formatted
from scrapers.scraper import SrealityScraper


class SrealityStubsScraper(SrealityScraper):
    __instance__ = None

    def __init__(self) -> None:
        super().__init__()
        self.stubs_collection_name = f'sreality.cz_house_stubs'

    def get_instance():
        if SrealityStubsScraper.__instance__ == None:
            SrealityStubsScraper.__instance__ = SrealityStubsScraper()
        return SrealityStubsScraper.__instance__

    def save_estates(self, estates):
        with self.get_connection() as connection:
            database = connection[self.database_name]
            self.create_mongo_collection_if_not_exists(database, self.stubs_collection_name)
            collection = database[self.stubs_collection_name]

            print(f'Saving raw data to [{self.stubs_collection_name}]')
            written_count = 0
            for estate in estates:
                hash_id = estate['hash_id']
                is_duplicate = {"hash_id": hash_id}
                item = {
                    'inserted_at': self.date_time_formatted(),
                    'data': estate,
                    'hash_id': hash_id
                }
                written = collection.update_one(is_duplicate, {"$setOnInsert": item}, upsert=True)
                if written.upserted_id:
                    written_count += 1
            print(f'Written: [{written_count:03d}] / [{len(estates):03d}] in total')

    def get_num_pages(self, per_page):
        url = f'https://www.sreality.cz/api/cs/v2/estates?category_main_cb=2&category_sub_cb=37|33|39|43&category_type_cb=1&no_auction=1&page=1&per_page=1&tms=1559845117714'
        data = self.make_get_request(url).json()
        results_size = data['result_size']
        return ceil(results_size / per_page)

    def scrape(self, per_page=60, start_page=1, max_pages=9999):
        found_count = self.get_num_pages(per_page)
        print(f'Maximal number of pages: [{found_count}]')
        total_pages = min(found_count, max_pages)
        for current_page in range(start_page, total_pages + 1):
            print(f'Current page being scraped [{current_page:04d}] of [{total_pages:04d}]')
            url = f'https://www.sreality.cz/api/cs/v2/estates?category_main_cb=2&category_sub_cb=37|33|39|43&category_type_cb=1&no_auction=1&page={current_page}&per_page={per_page}&tms=1559845117714'
            data = self.make_get_request(url).json()
            self.save_estates(data['_embedded']['estates'])
