import requests
from pymongo import MongoClient
from datetime import datetime
import gridfs

class SrealityScraper(object):
    def __init__(self) -> None:
        super().__init__()
        self.database_name = f'scraped'

    def get_connection(self):
        return MongoClient('mongodb://127.0.0.1:27000/')

    def create_mongo_collection_if_not_exists(self, database, collection_name, after_create = None):
        if collection_name not in database.list_collection_names():
            database.create_collection(collection_name, storageEngine={'wiredTiger': {'configString': 'block_compressor=zlib'}})
            if after_create is not None:
                after_create(database[collection_name])
            return True
        return False

    def scrape(self, *args, **kwargs):
        raise NotImplementedError('This method should be overridden!')

    def date_time_formatted(self):
        return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")

    def make_get_request(self, url):
        return requests.get(url)

    def save_file(self, db, data, **kwargs):
        fs = gridfs.GridFS(db)
        fileID = fs.put(data, **kwargs)
        return fileID

    def load_file(self, db, file_id, **kwargs):
        fs = gridfs.GridFS(db)
        fileInterface = fs.get(file_id, **kwargs)
        return fileInterface
