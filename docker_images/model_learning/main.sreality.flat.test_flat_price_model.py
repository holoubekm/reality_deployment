import sys
from os import path
MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", 'api_backend', 'app'))
sys.path.append(MOD_COMMON_PATH)
MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", 'api_backend'))
sys.path.append(MOD_COMMON_PATH)

from common.utils import Stopwatch
from models.price_model_inference import PriceModelInference

if __name__ == '__main__':
    sw = Stopwatch()
    with sw('Pregenerating data to be fed into the inferencer'):
        items = [
            {'feature_a': 0, 'feature_b': '', 'feature_c': False}
        ]

    m = PriceModelInference('house', 'house_price_model_2019-06-20_21-55-20_mae_epochs=250_bs=64_fc=231_lr=0.0005_drop=0.25', models_dir='../api_backend/app/models/saved/')
    for item in items:
        e = m.get_estimation(None, None, None, item, with_error_estimation=True)
        print(e)
