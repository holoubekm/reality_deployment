from common.utils import Stopwatch

from pymongo import MongoClient
from datetime import datetime

import gridfs
import os
import sys

from scrapers.sreality.flat.images_scraper import SrealityImagesScraper


if __name__ == '__main__':
    sw = Stopwatch()

    scraper = SrealityImagesScraper()
    with scraper.get_connection() as connection:
        db = connection[scraper.database_name]

        images_collection = db[scraper.images_collection_name]

        execution_path = os.getcwd()

        with sw(f'Loading object detection model'):
            from imageai.Detection import ObjectDetection
            detector = ObjectDetection()
            detector.setModelTypeAsRetinaNet()

            detector.setModelPath( os.path.join(execution_path , "models", 'object_detection', "resnet50_coco_best_v2.0.1.h5"))
            detector.loadModel()

        images = images_collection.find()
        for image in images:
            fileId = image["fileID"]
            if 'metadata' in image:
                print(f'skipping image {image["filename"]}')
                continue

            if 'metadata' not in image:
                image["metadata"] = {}


            if False and "objects" not in image["metadata"]:            
                with sw(f'Preprocessing image {image["filename"]} for quality recognition'):

                    imgfile = scraper.load_file(db,fileId)

                    try:

    #                    detections = detector.detectObjectsFromImage(input_image=imgfile, output_image_path=os.path.join(execution_path , "imagenew.jpg"))
                        detections = detector.detectObjectsFromImage(input_image=imgfile)
                        ####convert box_points to list so it can be stored in pymongo
                        detections = [ {**e, "box_points": e["box_points"].tolist()} for e in detections]

                        print(f"Updating detections field with {len(detections)} results")
                        image["metadata"]["objects"] = detections

                        images_collection.find_one_and_update({"_id": image["_id"]}, 
                                        {"$set": image})
                    except Exception as e:
                        print("!!! ERROR DURING OBJECT DETECTION")
                        print(str(e))
