from common.utils import Stopwatch

from pymongo import MongoClient
from datetime import datetime

import hashlib
import gridfs
import os
import sys

from scrapers.sreality.flat.images_scraper import SrealityImagesScraper


if __name__ == '__main__':
    sw = Stopwatch()

    scraper = SrealityImagesScraper()
    with scraper.get_connection() as connection:
        db = connection[scraper.database_name]

        images_collection = db[scraper.images_collection_name]
        features_collection = db[scraper.features_collection_name]

        execution_path = os.getcwd()


        def get_hash(s):
            hash_object = hashlib.sha512(s.encode('utf-8'))
            hex_dig = hash_object.hexdigest()
            return hex_dig

        

        features = features_collection.find()
        for feature in features:
            data = feature["data"]
            if data is not None and "sreal_stav_objektu_nom_string" in data:
                stav_objektu = data["sreal_stav_objektu_nom_string"]
                for imageurl in feature["images"]:
                    print(f"doing file {imageurl}")

                    imagerecord = images_collection.find_one({"filename": imageurl})
                    if imagerecord is not None:
                        fileobj = scraper.load_file(db,imagerecord["fileID"])

                        dirname = f'data_cache/imgquality/{stav_objektu}'

                        os.makedirs(dirname, exist_ok=True)

                        fout = open(f'{dirname}/{get_hash(imageurl)}.jpeg', 'wb')

                        fout.write(fileobj.read())

                        fileobj.close()
                        fout.close()

                        print(f"... done")
                    else:
                        print(f"... missing")

