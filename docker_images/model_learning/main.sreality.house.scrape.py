import sys
from os import path
MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", 'api_backend', 'app'))
sys.path.append(MOD_COMMON_PATH)

from common.utils import Stopwatch
from scrapers import SrealityHouseStubsScraper, SrealityHouseDetailsScraper, SrealityHouseFeaturesMiner

if __name__ == '__main__':

    sw = Stopwatch()
    # with sw('Scraping new stems from [sreality.cz] in a delta way'):
    #     stubs_scraper = SrealityHouseStubsScraper.get_instance()
    #     stubs_scraper.scrape(per_page=250, start_page=1, max_pages=9999)
    #     del stubs_scraper

    # with sw('Scraping details for previously saved stems from [sreality.cz]'):
    #     details_scraper = SrealityHouseDetailsScraper.get_instance()
    #     details_scraper.scrape()
    #     del details_scraper

    with sw('Mining features from the raw scraped data'):
        features_miner = SrealityHouseFeaturesMiner.get_instance()
        features_miner.mine()
        del features_miner
