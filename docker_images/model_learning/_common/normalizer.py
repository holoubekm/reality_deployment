from numpy import zeros as np_zeros

from common.exceptions import InternalException, MissingFeatureException, UnexpectedFeatureException, UnexpectedNominalValueException
from common.utils import load_config, date_time_formatted


class Normalizer(object):
    __instance__ = None

    def __init__(self, config_name='normalizer.json', config_dir='app/utils', config=None):
        super().__init__()
        if config is not None:
            self.config = config
        else:
            self.config = load_config(file_name=config_name, config_dir=config_dir)
    
        self.skip_data_check = False

        self.data = None
        self.constraints = {}

        # self.required_features = set()
        self.all_features = []

        self.nominal_synonyms = {}
        self.nominal_syn2ord = {}
        self.nominal_syn2hot_one = {}

        for feature in self.config['features']:
            # from pprint import pprint
            # pprint(feature)
            # exit()
            feature_id = feature['feature_id']
            self.constraints[feature_id] = feature

            assert feature_id not in self.all_features, f'The same feature multiple times across the config: [{feature_id}]'
            self.all_features.append(feature_id)
            # if feature['is_required']:
                # self.required_features.add(feature_id)

            if feature['variable_type'] == 'nominal':
                values = feature['values']
                self.nominal_synonyms[feature_id] = set()
                for i, value in enumerate(values):
                    synonyms = value['synonyms']
                    for synonym in synonyms:
                        assert synonym not in self.nominal_synonyms[feature_id], f'Duplicate synonym: [{synonym}] found in one feature: [{feature_id}]'
                        self.nominal_synonyms[feature_id].add(synonym)

                encoder = feature['encoder']
                if encoder == 'ordinal':
                    self.nominal_syn2ord[feature_id] = {}
                    for i, value in enumerate(values):
                        ordinal = value['ordinal']
                        synonyms = value['synonyms']
                        for synonym in synonyms:
                            self.nominal_syn2ord[feature_id][synonym] = ordinal
                elif encoder == 'one-hot':
                    if feature['data_type'] == 'boolean':
                        hot_one_false = np_zeros(shape=(2), dtype=int)
                        hot_one_true = np_zeros(shape=(2), dtype=int)
                        hot_one_null = np_zeros(shape=(2), dtype=int)

                        hot_one_true[0] = 1
                        hot_one_null[1] = 1

                        self.nominal_syn2hot_one[feature_id] = {
                            'false': hot_one_false, False: hot_one_false,
                            'true': hot_one_true, True: hot_one_true,
                            'null': hot_one_null, None: hot_one_null }

                    else:
                        self.nominal_syn2hot_one[feature_id] = {}
                        for i, value in enumerate(values):
                            synonyms = value['synonyms']
                            hot_one = np_zeros(shape=(len(values)), dtype=int)
                            hot_one[i] = 1
                            for synonym in synonyms:
                                self.nominal_syn2hot_one[feature_id][synonym] = hot_one
                else:
                    raise InternalException(f'Inconsistency found in the config file: Only [ordinal, one-hot] encoders are allowed. Found: [{encoder}]')
        # if len(self.required_features) <= 0:
            # raise InternalException('Inconsistency found in the config file. At least one feature must be flagged as required.')
        
        self.all_features_set = set(self.all_features)
        
        if self.skip_data_check:
            print('Warning: skip_check flag is enabled. Data will not be validated!')
        self.reset()


    def get_instance():
        if Normalizer.__instance__ == None:
            Normalizer.__instance__ = Normalizer()
        return Normalizer.__instance__


    def reset(self):
        self.data = {feature: [] for feature in self.all_features}

    def check_ordinal(self, feature_id, constraint, value):
        data_type = constraint['data_type']
        min_limit, max_limit = constraint['limits']['min'], constraint['limits']['max']
        if data_type == 'float':
            assert isinstance(value, float) or isinstance(value, int), f'Unexpected data type: [{type(value)}] of: [{feature_id}]'
        elif data_type == 'integer':
            assert isinstance(value, int), f'Unexpected data type: [{type(value)}] of: [{feature_id}]'
        else:
            raise InternalException(f'Unimplemented data type: [{data_type}]')

        if min_limit:
            assert min_limit <= value, f'The value of: [{feature_id}] is out of bounds. The following condition does not hold: [{min_limit}] <= [{value}]'
        if max_limit:
            assert value <= max_limit, f'The value of: [{feature_id}] is out of bounds. The following condition does not hold: [{value}] <= [{max_limit}]'

    def check_nominal(self, feature_id, value):
        if value not in self.nominal_synonyms[feature_id]:
            raise UnexpectedNominalValueException(f'Unexpected value: [{value}] found in: [{feature_id}]. Admissible values: [{self.nominal_synonyms[feature_id]}]')

    def check_feature(self, feature_id, value):
        if feature_id in self.constraints:
            constraint = self.constraints[feature_id]
            variable_type = constraint['variable_type']
            if variable_type == 'nominal':
                self.check_nominal(feature_id, value)
            elif variable_type == 'ordinal':
                self.check_ordinal(feature_id, constraint, value)
            else:
                raise InternalException(f'Unimplemented variable type: [{variable_type}]')
        else:
            raise UnexpectedFeatureException(f'Unexpected feature: [{feature_id}]')

    def fill_missing_data(self, not_added):
        for feature_id in not_added:
            constraint = self.constraints[feature_id]
            if_missing = constraint['if_missing']

            if not if_missing:
                raise InternalException(f'Inconsistency found in the Normalizer config file. If_missing field is null for: [{feature_id}]')

            method = if_missing['method']
            if method == "impute":
                value = if_missing['arg']
            elif method == "mean":
                value = constraint['properties']['mean']
            elif method == "error":
                raise MissingFeatureException(f'Missing value: [{feature_id}]')
            else:
                raise InternalException(f'Unimplemented missing data method: [{method}]')
            self.data[feature_id].append(value)

    def add_row(self, features):
        added_features = set()
        for feature_id in features:
            value = features[feature_id]
            added_features.add(feature_id)
            if not self.skip_data_check:
                self.check_feature(feature_id, value)
            self.data[feature_id].append(value)

        # unsatisfied = self.required_features.difference(added_features)
        # if len(unsatisfied):
            # raise MissingFeatureException(f'Some required features were not satisfied: [{unsatisfied}]')

        not_added = self.all_features_set.difference(added_features)
        self.fill_missing_data(not_added)

    def normalize_feature(self, feature_id, values, constraint):
        variable_type = constraint['variable_type']

        if variable_type == 'nominal':
            return values
        elif variable_type == 'ordinal':
            normalizer = constraint['normalizer']
            if not normalizer:
                return values
            properties = constraint['properties']
            if normalizer == 'z-score':
                mean_value = properties['mean']
                std_value = properties['std']
                if std_value != 0:
                    return [(value - mean_value) / std_value for value in values]
                return values
            elif normalizer == 'min-max':
                min_value = properties['min']
                max_value = properties['max']
                extent = max_value - min_value
                if min_value >= max_value:
                    raise InternalException(
                        f'There is some inconsistency in the data! Min value must be lower than max value: [{feature_id}] - [{min_value}] x [{max_value}]')
                return [(value - min_value) / extent for value in values]
            else:
                raise InternalException(f'Not implemented normalizer: [{normalizer}]')
        else:
            raise InternalException(f'Not implemented variable type: [{variable_type}]')

    def normalize_batch(self):
        for feature_id in self.all_features:
            constraint = self.constraints[feature_id]
            values = self.normalize_feature(feature_id, self.data[feature_id], constraint)
            self.data[feature_id] = values

    def encode_feature(self, feature_id, values, constraint):
        variable_type = constraint['variable_type']
        data_type = constraint['data_type']

        if variable_type == 'nominal':
            encoder = constraint['encoder']
            if not encoder:
                return {feature_id: values}

            if data_type == 'boolean':
                if encoder == 'ordinal':
                    return {feature_id: [self.nominal_syn2ord[feature_id][value] for value in values]}
                elif encoder == 'one-hot':
                    postfix = {0: '=true', 1: '=null'}
                    rows = len(values)
                    cols = 2
                    lookup_table = self.nominal_syn2hot_one[feature_id]
                    data = np_zeros(shape=(rows, cols), dtype=int)
                    for row in range(rows):
                        value = values[row]
                        data[row][:] = lookup_table[value]
                    return {f'{feature_id}_{postfix[col]}': data[:, col].tolist() for col in range(cols)}
                else:
                    raise InternalException(f'Unimplemented encoder: [{encoder}] for type: [{data_type}]')
            elif data_type == 'string':
                if encoder == 'ordinal':
                    return {feature_id: [self.nominal_syn2ord[feature_id][value] for value in values]}
                elif encoder == 'one-hot':
                    rows = len(values)
                    cols = len(constraint['values'])
                    lookup_table = self.nominal_syn2hot_one[feature_id]
                    data = np_zeros(shape=(rows, cols), dtype=int)
                    for row in range(rows):
                        value = values[row]
                        data[row][:] = lookup_table[value]
                    return {f'{feature_id}={value}': data[:, col].tolist() for col in range(cols)}
                else:
                    raise InternalException(f'Unimplemented encoder: [{encoder}] for type: [{data_type}]')
            else:
                raise InternalException(f'Unknown data type: [{data_type}]')
        elif variable_type == 'ordinal':
            return {feature_id: values}
        else:
            raise InternalException(f'Not implemented variable type: [{variable_type}]')

    def encode_batch(self):
        data = {}
        feature_mapping = {}
        for feature_id in self.all_features:
            constraint = self.constraints[feature_id]
            values = self.encode_feature(feature_id, self.data[feature_id], constraint)
            data.update(values)
            feature_mapping[feature_id] = list(values.keys())

        # This is crucial - keep everything in the order!
        # data = {key: data[key] for key in sorted(data)}
        batch = {
            'generated_at': date_time_formatted(),
            'feature_mapping': feature_mapping,
            'data': data
        }
        return batch

    def normalize(self):
        self.normalize_batch()
        batch = self.encode_batch()
        self.reset()
        return batch
