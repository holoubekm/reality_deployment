import sys
from os import path
MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", 'api_backend', 'app'))
sys.path.append(MOD_COMMON_PATH)

from common.utils import Stopwatch
from models.flat_price_model_inference import FlatPriceModelInference

import requests
from uuid import uuid4
from json import loads as json_loads
from time import sleep
from celery import Celery, signature, group

from common.utils import Stopwatch
from scrapers.scraper import SrealityScraper

app = Celery()
app.config_from_object('celeryconfig')



if __name__ == '__main__':
    sw = Stopwatch()
    # with sw('Pregenerating data to be fed into the inferencer'):
    #     items = [
    #         {'feature_a': 0, 'feature_b': '', 'feature_c': False},
    #         {'feature_a': 1, 'feature_b': 'a', 'feature_c': True},
    #         {'feature_a': 2, 'feature_b': 'b', 'feature_c': False},
    #         {'feature_a': 3, 'feature_b': 'c', 'feature_c': True},
    #         {'feature_a': 4, 'feature_b': 'd', 'feature_c': False},
    #         {'feature_a': 5, 'feature_b': 'd', 'feature_c': True},
    #         {'feature_a': 6, 'feature_b': 'c', 'feature_c': False},
    #         {'feature_a': 7, 'feature_b': 'b', 'feature_c': True},
    #         {'feature_a': 8, 'feature_b': 'a', 'feature_c': False},
    #         {'feature_a': 9, 'feature_b': '', 'feature_c': True}
    #     ]


    description = "Nabízíme k prodeji slunný byt 3+1 o celkové výměře 75m2. Byt je v OV, se zaskleným balkonem v 6. patře ( z 12ti). Lokalita u jablonecké přehrady v městské části Mšeno.\r\n\r\nDispozice bytu dle fotografií - chodba s vestavěnou skříní, ložnice, koupelna s rohovou vanou, dětský pokoj, obývací pokoj, kuchyně. Plovoucí podlahy, byt je po celkové rekonstrukci.\r\n\r\nByt se nachází v panelovém domě, jsou tu nové výtahy, vlastní plynová kotelna, plastová okna. Díky vlastní plynové kotelně jsou i výrazně nižší provozní náklady. V blízkosti veškerá občanská vybavenost. Parkování před domem. \r\n\r\nPo nové rychlostní silnici v Liberci za 10 minut. Koupání v jablonecké přehradě 3 minuty pěšky, v lese do 10ti minut pěšky. Prohlídku doporučujeme.\r\n\r\nNastěhování dle domluvy, ideálně září / říjen. \r\n\r\nProsíme ať nevolají RK, děkuji"
    lon, lat = 15.1678196635, 50.7381206519
    
    external_api_data = {
        'sreal_stav_nom_string': 'rezervovano',
        'sreal_stavba_nom_string': 'panelova',
        'sreal_stav_objektu_nom_string': 'dobry',
        'sreal_vlastnictvi_nom_string': 'osobni',
        'sreal_uzitna_plocha_ord_float': 75,
        'sreal_plocha_podlahova_ord_float': 75,
        'sreal_balkon_nom_boolean': "true",
        'sreal_sklep_nom_boolean': "true",
        'sreal_voda_dalkovy_vodovod_nom_boolean': "true",
        'sreal_topeni_lokalni_plynove_nom_boolean': "true",
        'sreal_telekomunikace_telefon_nom_boolean': "true",
        'sreal_telekomunikace_internet_nom_boolean': "true",
        'sreal_telekomunikace_kabelova_televize_nom_boolean': "true",
        'sreal_elektrina_230v_nom_boolean': "true",
        "sreal_doprava_vlak_nom_boolean": "true",
        "sreal_doprava_dalnice_nom_boolean": "true",
        "sreal_doprava_silnice_nom_boolean": "true",
        "sreal_doprava_autobus_nom_boolean": "true",
        "sreal_doprava_mhd_nom_boolean": "true",
        "sreal_vytah_nom_boolean": "true"
    }


    kwargs = {
        'lon': lon,
        'lat': lat,
        'description': description
    }
    task = signature('app.main_api_worker._internal_fetch_additional_data', kwargs=kwargs)
    result = task.apply_async()
    internal_data = result.get()

    external_api_data.update(internal_data)


    m = FlatPriceModelInference(models_dir='../api_backend/app/models/saved/')
    m.load_keras_model('2019-02-03_12-52-40_ll_mean_absolute_error_epochs=250_bs=64_fc=623_lr=0.0005_kr=l2_drop=0.25')
    e = m.get_estimation(None, None, None, external_api_data, with_error_estimation=True)
    print(e)
    # print(e['prediction'])

