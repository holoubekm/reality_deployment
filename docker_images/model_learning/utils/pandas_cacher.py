from pandas import read_parquet
from json import dump as json_dump
from json import load as json_load
from os import path, makedirs

from common.utils import Stopwatch

class PandasCacher(object):

    def __init__(self, name, cache_dir='data_cache', metadata={}, **kwargs) -> None:
        super().__init__()
        self.all_metadata = dict(metadata)
        self.all_metadata.update(kwargs)
        self.cache_dir = cache_dir

        params = '_'.join([f'{key}={val}' for key, val in kwargs.items()]).lower()
        cache_file_name = f'{name}_{params}'
        cache_data_file = f'{cache_file_name}.parquet.br'
        cache_meta_file = f'{cache_file_name}.meta.json'
        self.cache_data_file_path = path.join(cache_dir, cache_data_file)
        self.cache_meta_file_path = path.join(cache_dir, cache_meta_file)
        self.sw = Stopwatch()

    def load_from_cache(self):
        if not path.exists(self.cache_dir):
            makedirs(self.cache_dir)

        if path.exists(self.cache_data_file_path) and path.exists(self.cache_meta_file_path):
            with self.sw(f'Loading data from the cache: [{self.cache_data_file_path}]'):
                with open(self.cache_meta_file_path, 'r', encoding='utf-8') as in_file:
                    self.all_metadata = json_load(in_file)
                return read_parquet(self.cache_data_file_path), self.all_metadata
        else:
            print(f'Cache file not found: [{self.cache_data_file_path}] or [{self.cache_meta_file_path}]')
        df, metadata = self.get_data()
        self.all_metadata.update(metadata)

        with open(self.cache_meta_file_path, 'w', encoding='utf-8') as out_file:
            json_dump(self.all_metadata, out_file, sort_keys=True, indent=4)

        df.to_parquet(self.cache_data_file_path, compression='brotli')
        return df, self.all_metadata

    def get_data(self):
        raise NotImplementedError('This method should be overridden!')
