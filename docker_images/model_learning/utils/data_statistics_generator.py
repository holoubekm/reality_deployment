import numpy as np
from json import dump as json_dump
from math import sqrt
from os import path, makedirs

from common.utils import text_lower_rm_accents_rm_ws, load_config, date_time_formatted, text_lower_rm_accents


class DataStatisticsGenerator(object):

    def __init__(self, config):
        super().__init__()
        self.config = load_config(config, config_dir='configs')
        self.data_set = {'ordinal': {'integer': {}, 'float': {}},
                         'nominal': {'string': {}, 'integer': {}, 'boolean': {}}}

        self.import_functions_map = {
            "ordinal": {
                "integer": self.import_ordinal_integer,
                "float": self.import_ordinal_float
            },
            "nominal": {
                "string": self.import_nominal_string,
                "integer": self.import_nominal_integer,
                "boolean": self.import_nominal_boolean
            }
        }

        self.features = self.config['features']
        self.check_config_validity = True
        self.data_row_count = 0


    def import_nominal_string(self, name, value, config):
        assert isinstance(value, str), f'Expected type is a string: [{name}] - [{value}]'
        feature_id = text_lower_rm_accents_rm_ws(name)
        value_norm = text_lower_rm_accents(value)

        data_set = self.data_set['nominal']['string']
        if feature_id not in data_set:
            data_set[feature_id] = {'feature_id': feature_id, 'name_full': name, 'added_to_index': {None: 0, '': 0}, 'values': [{'ordinal': 0, 'synonyms': {None, ''}}], 'config': config}

        added_to_index = data_set[feature_id]['added_to_index']
        values = data_set[feature_id]['values']

        if value_norm not in added_to_index:
            index = len(values)
            added_to_index[value_norm] = index
            values.append({'ordinal': index, 'synonyms': set()})

        index = added_to_index[value_norm]
        values[index]['synonyms'].update({value_norm})

    def import_nominal_integer(self, name, value, config):
        assert isinstance(value, int), f'Expected type is an integer: [{name}] - [{value}]'
        feature_id = text_lower_rm_accents_rm_ws(name)

        data_set = self.data_set['nominal']['integer']
        if feature_id not in data_set:
            data_set[feature_id] = {'feature_id': feature_id, 'name_full': name, 'added_to_index': {None: 0}, 'values': [{'ordinal': 0, 'synonyms': {None}}], 'config': config}

        added_to_index = data_set[feature_id]['added_to_index']
        values = data_set[feature_id]['values']

        if value not in added_to_index:
            index = len(values)
            added_to_index[value] = index
            values.append({'ordinal': index, 'synonyms': set()})
        index = added_to_index[value]
        values[index]['synonyms'].update({value})

    def import_nominal_boolean(self, name, value, config):
        assert isinstance(value, bool) or isinstance(value, str), f'Expected type is boolean or string: [{name}] - [{value}]'
        feature_id = text_lower_rm_accents_rm_ws(name)
        value_norm = value

        data_set = self.data_set['nominal']['boolean']
        if feature_id not in data_set:
            data_set[feature_id] = {
                'feature_id': feature_id, 
                'name_full': name, 
                'added_to_index': {
                    'false': 0, 
                    False: 0,
                    'true': 1,
                    True: 1,
                    'null': 2,
                    None: 2,
                },
                'values': [
                    {'ordinal': 0, 'synonyms': {'false', False}},
                    {'ordinal': 1, 'synonyms': {'true', True}},
                    {'ordinal': 2, 'synonyms': {'null', None}}
                ], 
                'config': config
            }

        added_to_index = data_set[feature_id]['added_to_index']
        values = data_set[feature_id]['values']

        if value_norm not in added_to_index:
            index = len(values)
            added_to_index[value_norm] = index
            values.append({'ordinal': index, 'synonyms': set()})
        index = added_to_index[value_norm]
        values[index]['synonyms'].update({value_norm})

    def import_ordinal_integer(self, name, value, config):
        assert isinstance(value, int), f'Expected type is an integer: [{name}] - [{value}]'
        feature_id = text_lower_rm_accents_rm_ws(name)
        data_set = self.data_set['ordinal']['integer']
        if feature_id not in data_set:
            # data_set[feature_id] = {'feature_id': feature_id, 'name_full': name, 'values': [], 'config': config}
            data_set[feature_id] = {'feature_id': feature_id, 'name_full': name, 'sum': 0, 'count': 0, 'sq_sum': 0, 'max': float('-inf'), 'min': float('inf'), 'config': config}
        # values = data_set[feature_id]['values']
        # values.append(value)
        data_set[feature_id]['sum'] += value
        data_set[feature_id]['sq_sum'] += value * value
        data_set[feature_id]['count'] += 1
        data_set[feature_id]['min'] = min(value, data_set[feature_id]['min'])
        data_set[feature_id]['max'] = max(value, data_set[feature_id]['max'])

    def import_ordinal_float(self, name, value, config):
        assert isinstance(value, float) or isinstance(value, int), f'Expected type is a float: [{name}] - [{value}]'
        feature_id = text_lower_rm_accents_rm_ws(name)
        data_set = self.data_set['ordinal']['float']
        if feature_id not in data_set:
            # data_set[feature_id] = {'feature_id': feature_id, 'name_full': name, 'values': [], 'config': config}
            data_set[feature_id] = {'feature_id': feature_id, 'name_full': name, 'sum': 0, 'count': 0, 'sq_sum': 0, 'max': float('-inf'), 'min': float('inf'), 'config': config}
        # values = data_set[feature_id]['values']
        # values.append(value)
        data_set[feature_id]['sum'] += value
        data_set[feature_id]['sq_sum'] += value * value
        data_set[feature_id]['count'] += 1
        data_set[feature_id]['min'] = min(value, data_set[feature_id]['min'])
        data_set[feature_id]['max'] = max(value, data_set[feature_id]['max'])

    def get_ordinal_feature(self, feature_data):
        feature = {
            'feature_id': feature_data['feature_id'],
            'data_type': feature_data['config']['data_type'],
            'variable_type': feature_data['config']['variable_type'],
            'normalizer': feature_data['config']['normalizer'],
            'if_missing': feature_data['config']['if_missing'],
            'limits': feature_data['config']['limits']
        }

        # values = feature_data['values']
        # feature['properties'] = {
        #     'min': min(values),
        #     'max': max(values),
        #     'mean': np.mean(values),
        #     'var': np.var(values),
        #     'std': np.std(values)
        # }

        mean = feature_data['sum'] / feature_data['count']
        sq_mean = feature_data['sq_sum'] / feature_data['count']
        var = sq_mean - (mean*mean)
        feature['properties'] = {
            'min': feature_data['min'],
            'max': feature_data['max'],
            'mean': mean,
            'var': var,
            'std': sqrt(var)
        }
        return feature

    def get_nominal_feature(self, feature_data):
        feature = {
            'feature_id': feature_data['feature_id'],
            'data_type': feature_data['config']['data_type'],
            'variable_type': feature_data['config']['variable_type'],
            'encoder': feature_data['config']['encoder'],
            'if_missing': feature_data['config']['if_missing'],
        }

        values = []
        for value in feature_data['values']:
            values.append({
                'ordinal': value['ordinal'],
                'synonyms': list(value['synonyms'])
            })
        feature['values'] = values
        return feature

    @staticmethod
    def check_config(config):
        for feature in config['features']:
            assert feature['data_type'] in ['integer', 'float', 'string', 'boolean'], f'Unknown data type: [{feature["data_type"]}]'
            assert feature['variable_type'] in ['ordinal', 'nominal'], f'Unknown variable type: [{feature["variable_type"]}]'
            if feature['variable_type'] == 'nominal':
                bucket = set()
                for values in feature['values']:
                    for synonym in values['synonyms']:
                        assert synonym not in bucket, f'Synonyms must be unique among different ordinal values: [{synonym}]'
                        bucket.add(synonym)

            # TODO incorporate this to tests
            # elif feature['variable_type'] == 'ordinal':
            #     limit_min = feature['limits']['min']
            #     limit_max = feature['limits']['max']
            #     property_min = feature['properties']['min']
            #     property_max = feature['properties']['max']
            #     assert limit_min <= property_min <= property_max <= limit_max, f'There is some inconsistency between data and enforced limits: [{limit_min} <= [{property_min} <= [{property_max} <= [{limit_max}]'

    def get_feature_config(self, feature_id, value):
        assert feature_id in self.features, f'Unexpected column: [{feature_id}] not found in the config. Value: [{value}]'
        return self.features[feature_id]

    def get_import_function(self, data_type, variable_type):
        assert variable_type in self.import_functions_map, f'Unexpected variable_type: [{variable_type}] not found in the function map.'
        data_map = self.import_functions_map[variable_type]
        assert data_type in data_map, f'Unexpected data_type: [{data_type}] not found in the function map.'
        return data_map[data_type]

    def add_row(self, data):
        assert isinstance(data, dict)
        for feature_id, value in data.items():
            config = self.get_feature_config(feature_id, value)
            data_type = config['data_type']
            variable_type = config['variable_type']

            fn = self.get_import_function(data_type, variable_type)
            fn(feature_id, value, config)
        self.data_row_count += 1

    def is_feature_in_config(self, feature_id):
        return feature_id in self.features
    
    def generate_config(self, output_file):
        features = []
        ordinals = self.data_set['ordinal']
        for data_type in ordinals:
            ordinal = ordinals[data_type]
            for feature_data in ordinal.values():
                features.append(self.get_ordinal_feature(feature_data))


        nominals = self.data_set['nominal']
        for data_type in nominals:
            nominal = nominals[data_type]
            for feature_data in nominal.values():
                features.append(self.get_nominal_feature(feature_data))

        config = {}
        config["date_time"] = date_time_formatted()
        config["features"] = features
        config['total_rows'] = self.data_row_count
        config['total_features'] = len(features)

        if self.check_config_validity:
            self.check_config(config)

        config_dir = 'configs/generated/'
        if not path.exists(config_dir):
            makedirs(config_dir)
        output_path = path.join(config_dir, output_file)

        with open(output_path, 'w', encoding='utf-8') as out_file:
            json_dump(config, out_file, sort_keys=True, indent=4)

        print(f'Generated normalizer config was saved to: [{output_path}]')
