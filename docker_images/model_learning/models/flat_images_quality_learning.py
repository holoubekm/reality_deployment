import os

class FlatImgQualityLearning(object):

    def __init__(self):
        self.img_size = (224,224)
        self.label_csv = './data_cache/imgquality.csv'
        pass

    def extract_images(self):
        import shutil
        from pandas import read_csv
        inplabels = read_csv(self.label_csv)
        
        #select only neccesary columns
        inplabels = inplabels[inplabels.Label != "Skip"]
        inplabels = inplabels.loc[:, ['External ID', 'Label', "Dataset Name"]]

        new_datas = inplabels.aggregate(lambda arr: (
                            arr["External ID"], 
                            os.path.join(FlatImgQualityLearning.getDatasetDirectory(arr["Dataset Name"]), arr["External ID"])
                        ),axis=1)

        for el in new_datas:
            (filename, filepath) = el
            shutil.copy2(
                    os.path.join(os.getcwd(), 'data_cache', 'imgquality', filepath), 
                    os.path.join(os.getcwd(), 'data_cache', 'imgquality_extracted', filename)
            )
        

    @staticmethod
    def getDatasetDirectory(ds):
        datasets = {
            "good_1": "dobry"
        }
        return datasets[ds]

    def learn_model(self, model):

        from tensorflow.python.keras.applications.mobilenet import preprocess_input
        from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
        from pandas import read_csv, Series
        import numpy as np
        import json


        inplabels = read_csv(self.label_csv)
        
        #select only neccesary columns
        inplabels = inplabels[inplabels.Label != "Skip"]
        inplabels = inplabels.loc[:, ['External ID', 'Label']]


        def preprocess_data(row):
            qualityTypes = {
                "bad": 0,
                "moderate": 0.25,
                "good": 0.5,
                "excellent": 0.75,
                "luxury": 1,
                "undefined": 0.5,
            }

            return Series([
                        qualityTypes[row["imgquality"]]
                    ], dtype='float64')

        #convert labels JSON to inputs
        inplabels['Label'] = inplabels['Label'].apply(lambda text: preprocess_data(json.loads(text)))


#        inplabels['External ID'] = inplabels.aggregate(lambda arr: os.path.join(FlatImgQualityLearning.getDatasetDirectory(arr["Dataset Name"]), arr["External ID"]),axis=1)
#        inplabels['External ID'] = inplabels['External ID'].apply(lambda url: )

        train_datagen=ImageDataGenerator(preprocessing_function=preprocess_input) #included in our dependencies

        cwd = os.getcwd()
        train_generator=train_datagen.flow_from_dataframe(dataframe = inplabels, 
                                                        directory = os.path.join(os.getcwd(), "data_cache", "imgquality_extracted"),
                                                        x_col = "External ID",
                                                        y_col = "Label",
                                                        target_size=(224,224),
                                                        color_mode='rgb',
                                                        batch_size=32,
                                                        class_mode='other',
                                                        shuffle=True)
        model.compile(optimizer='Adam',loss='mean_squared_error',metrics=['accuracy'])

        step_size_train=train_generator.n//train_generator.batch_size
        model.fit_generator(generator=train_generator,
                                steps_per_epoch=step_size_train,
                                epochs=10)

    def load_model(self, path):
        from tensorflow.keras.models import load_model
        from tensorflow.python.keras.initializers import glorot_uniform
        from tensorflow.keras.utils import custom_object_scope
        from tensorflow.keras.models import model_from_json
        with open(f"{path}.json", 'r') as json_file:
            loaded_model_json = json_file.read()
            model = model_from_json(loaded_model_json)

            model.load_weights(f"{path}.h5")

            return model

    def predict(self, model, path):
        import cv2
        import numpy as np
        from keras.applications.mobilenet import preprocess_input
        from keras.preprocessing.image import img_to_array, array_to_img
        image = cv2.imread(path)
        image = cv2.resize(image, self.img_size)
        x = np.asarray([img_to_array(image)])

        x = preprocess_input(x)

#        x = np.asarray([img_to_array(array_to_img(im, scale=False).resize((im_h,im_h))) for im in x])


        ret = model.predict(x)

        ret = [{"indoors": val[0], "outdoors": val[1]} for val in ret]

        return ret[0]
   
    def get_model(self):

        from keras.layers import Dense,GlobalAveragePooling2D
#        from keras.applications import VGG16, VGG19
        from keras.applications import MobileNet
        from keras.preprocessing import image

        from keras.models import Model

        base_model=MobileNet(weights='imagenet',input_shape=(224,224,3),include_top=False) #imports the mobilenet model and discards the last 1000 neuron layer.

        x=base_model.output
        x=GlobalAveragePooling2D()(x)
        x=Dense(1024,activation='relu')(x) #we add dense layers so that the model can learn more complex functions and classify for better results.
        preds=Dense(1,activation='softmax')(x) #final layer with softmax activation

        model=Model(inputs=base_model.input,outputs=preds)

        return model

