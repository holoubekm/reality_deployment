
class FlatPriceQualityLearning(object):

    def __init__(self):
        self.img_size = (224,224)
        pass

    def data_to_pickle(self):

        from keras.applications.mobilenet import preprocess_input
        from keras.preprocessing.image import ImageDataGenerator

        train_datagen=ImageDataGenerator(preprocessing_function=preprocess_input) #included in our dependencies
        train_generator=train_datagen.flow_from_directory('./data_cache/imgtype/', # this is where you specify the path to the main data folder
                                                        target_size=(224,224),
                                                        color_mode='rgb',
                                                        batch_size=32,
                                                        class_mode='categorical',
                                                        shuffle=True)
        print(train_generator)

    def load_model(self, path):
        from tensorflow.keras.models import load_model
        from tensorflow.python.keras.initializers import glorot_uniform
        from tensorflow.keras.utils import custom_object_scope
        from tensorflow.keras.models import model_from_json
        with open(f"{path}.json", 'r') as json_file:
            loaded_model_json = json_file.read()
            model = model_from_json(loaded_model_json)

            model.load_weights(f"{path}.h5")

            return model

    def predict(self, model, path):
        import cv2
        import numpy as np
        from keras.applications.mobilenet import preprocess_input
        from keras.preprocessing.image import img_to_array, array_to_img
        image = cv2.imread(path)
        image = cv2.resize(image, self.img_size)
        x = np.asarray([img_to_array(image)])

        x = preprocess_input(x)

#        x = np.asarray([img_to_array(array_to_img(im, scale=False).resize((im_h,im_h))) for im in x])


        ret = model.predict(x)

        ret = [{"indoors": val[0], "outdoors": val[1]} for val in ret]

        return ret[0]


    def learn_model(self, model):
        from keras.applications.mobilenet import preprocess_input
        from keras.preprocessing.image import ImageDataGenerator
        from keras.optimizers import Adam

        train_datagen=ImageDataGenerator(preprocessing_function=preprocess_input) #included in our dependencies
        train_generator=train_datagen.flow_from_directory('./data_cache/imgtype/', # this is where you specify the path to the main data folder
                                                        target_size=(224,224),
                                                        color_mode='rgb',
                                                        batch_size=32,
                                                        class_mode='categorical',
                                                        shuffle=True)

	

        model.compile(optimizer='Adam',loss='categorical_crossentropy',metrics=['accuracy'])
        # Adam optimizer
        # loss function will be categorical cross entropy
        # evaluation metric will be accuracy

        step_size_train=train_generator.n//train_generator.batch_size
        model.fit_generator(generator=train_generator,
                                steps_per_epoch=step_size_train,
                                epochs=10)
    
    def get_model(self):

        from keras.layers import Dense,GlobalAveragePooling2D
#        from keras.applications import VGG16, VGG19
        from keras.applications import MobileNet
        from keras.preprocessing import image

        from keras.models import Model

        base_model=MobileNet(weights='imagenet',input_shape=(224,224,3),include_top=False) #imports the mobilenet model and discards the last 1000 neuron layer.

        x=base_model.output
        x=GlobalAveragePooling2D()(x)
        x=Dense(1024,activation='relu')(x) #we add dense layers so that the model can learn more complex functions and classify for better results.
        x=Dense(1024,activation='relu')(x) #dense layer 2
        x=Dense(512,activation='relu')(x) #dense layer 3
        preds=Dense(1,activation='softmax')(x) #final layer with softmax activation

        model=Model(inputs=base_model.input,outputs=preds)

        return model

