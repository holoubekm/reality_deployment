import sys
from os import path
MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", 'api_backend', 'app'))
sys.path.append(MOD_COMMON_PATH)

from common.utils import Stopwatch, date_time_formatted
from common.normalizer import Normalizer
from models.price_model_learning import PriceModelLearning

from utils import PandasCacher
import pandas as pd

from scrapers.scraper import SrealityScraper

class DataCacher(PandasCacher):

    def __init__(self, normalizer_config, max_rows_count, skip_rows_count=0) -> None:
        kwargs = {
            'max_rows_count': max_rows_count,
            'skip_rows_count': skip_rows_count,
        }
        metadata = {
            'normalizer_config': normalizer_config
        }
        super().__init__(name='house_price_model_data', metadata=metadata, **kwargs)
        self.max_rows_count = max_rows_count
        self.normalizer_config = normalizer_config
        self.skip_rows_count = skip_rows_count


    def get_data(self):
        print(f'Loading top [{self.max_rows_count}] cached entries from the [sreality.cz]')
        scraper = SrealityScraper()
        with scraper.get_connection() as connection:
            db = connection[scraper.database_name]
            features_collection_name = f'sreality.cz_house_fetched'
            features_collection = db[features_collection_name]

            items = features_collection.find({}).limit(self.max_rows_count).skip(self.skip_rows_count)
            total_count = items.count(True)
            print(f'Total number to be fetched: [{total_count}]')

            prices = []
            with self.sw('Adding data to the normalizer'):
                normalizer = Normalizer(self.normalizer_config, config_dir='configs/generated/')
                for item in items:
                    normalizer.add_row(item['data'])
                    prices.append(item['price'])

        with self.sw('Runing the normalizer'):
            res = normalizer.normalize()
            data = res['data']
            data['price'] = prices
            metadata = {
                'feature_mapping': res['feature_mapping'],
                'normalizer_config': normalizer.config
            }
            return pd.DataFrame(data), metadata


if __name__ == '__main__':
    normalizer_config = 'normalizer.house_price_estimator.json' 
    cacher = DataCacher(normalizer_config, max_rows_count=20000)
    df, metadata = cacher.load_from_cache()
    del cacher

    normalizer_config = metadata['normalizer_config']
    feature_mapping = metadata['feature_mapping']

    m = PriceModelLearning('house', 'price_model_learning.house_price_estimator.json', normalizer_config, feature_mapping)
    m.set_learning_data(df)
    del df
    m.start_learning()
    m.save_model(models_dir='../api_backend/app/models/saved/')
