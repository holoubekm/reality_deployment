# import sys
from os import path
# MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", 'api_backend', 'app'))
# sys.path.append(MOD_COMMON_PATH)

# from common.utils import Stopwatch, date_time_formatted
# from common.normalizer import Normalizer
# from models.flat_price_model_learning import FlatPriceModelLearning

# from utils import PandasCacher
# import pandas as pd

# from scrapers.scraper import SrealityScraper

if __name__ == '__main__':

    pth = '../api_backend/app/models/saved/2019-02-03_12-52-40_ll_mean_absolute_error_epochs=250_bs=64_fc=623_lr=0.0005_kr=l2_drop=0.25/'

    import keras.backend as K
    from keras.models import model_from_json

    with open(path.join(pth, 'keras_model.json'), 'r', encoding='utf-8') as inp_file:
        keras_model = model_from_json(inp_file.read())
    keras_model.load_weights(path.join(pth, 'keras_model_weights.h5'))
    # keras_model.save(path.join(pth, 'keras_model.tf.h5'))

    # The export path contains the name and the version of the model
    # import tensorflow as tf
    # tf.keras.backend.set_learning_phase(0)  # Ignore dropout at inference
    # model = tf.keras.models.load_model(path.join(pth, 'keras_model.tf.h5'))
    export_path = '../api_backend/app/models/saved/2019-02-03_12-52-40_ll_mean_absolute_error_epochs=250_bs=64_fc=623_lr=0.0005_kr=l2_drop=0.25/1/'

    # Fetch the Keras session and save the model
    # The signature definition is defined by the input and output tensors
    # And stored with the default serving key
    # with tf.keras.backend.get_session() as sess:
    #     tf.saved_model.simple_save(sess, export_path, inputs={'input_image': model.input}, outputs={t.name: t for t in model.outputs})


    import tensorflow as tf

    classification_inputs = tf.saved_model.utils.build_tensor_info(keras_model.input)
    classification_outputs_classes = tf.saved_model.utils.build_tensor_info(keras_model.output)
    classification_signature = (
        tf.saved_model.signature_def_utils.build_signature_def(
            inputs={tf.saved_model.signature_constants.REGRESS_INPUTS: classification_inputs},
            outputs={tf.saved_model.signature_constants.REGRESS_OUTPUTS: classification_outputs_classes},
            method_name=tf.saved_model.signature_constants.REGRESS_METHOD_NAME)
        )
    # print("classification_signature content:")
    # print(classification_signature)

    from tensorflow.python.saved_model import builder as saved_model_builder
    from tensorflow.python.saved_model import utils
    from tensorflow.python.saved_model import tag_constants, signature_constants
    from tensorflow.python.saved_model.signature_def_utils_impl import     build_signature_def, predict_signature_def, regression_signature_def
    from tensorflow.contrib.session_bundle import exporter

    builder = saved_model_builder.SavedModelBuilder(export_path)

    # signature1 = predict_signature_def(inputs={'inputs': keras_model.input},
    #                                   outputs={'outputs': keras_model.output})

    # signature2 = regression_signature_def(examples={'inputs': keras_model.input},
                                      # predictions={'outputs': keras_model.output})


    with K.get_session() as sess:
        builder.add_meta_graph_and_variables(sess=sess,
                                             tags=[tag_constants.SERVING],
                                             signature_def_map={signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: classification_signature})
        builder.save()