import sys
from os import path
MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", 'api_backend', 'app'))
sys.path.append(MOD_COMMON_PATH)

from common.utils import Stopwatch
from utils import DataStatisticsGenerator
from scrapers.scraper import SrealityScraper
from scrapers import SrealityFlatRemoteFeaturesFetcher

if __name__ == '__main__':
    sw = Stopwatch()
    with sw('Fetching additional features for [sreality.cz] data from the API'):
        features_fetcher = SrealityFlatRemoteFeaturesFetcher.get_instance()
        features_fetcher.fetch()
        
    with sw('Pregenerating the normalizer config based on scraped data'):
        config_generator = DataStatisticsGenerator('data_statistics_generator.flat_price_estimator.json')
        
        scraper = SrealityScraper()
        with scraper.get_connection() as connection:
            db = connection[scraper.database_name]
            features_collection_name = f'sreality.cz_flat_fetched'
            features_collection = db[features_collection_name]

            items = features_collection.find({})

            not_found = set()
            for row in items:
                for feature in row['data']:
                    if not config_generator.is_feature_in_config(feature):
                        not_found.add(feature)

            if len(not_found):
                print(f'Features not found in the config:')
                print(not_found)
                print('Exiting...')
                exit()

            items.rewind()
            for row in items:
                config_generator.add_row(row['data'])
        config_generator.generate_config(output_file='normalizer.flat_price_estimator.json')
