from common.utils import Stopwatch

from pymongo import MongoClient
from datetime import datetime

from models.flat_images_quality_learning import FlatImgQualityLearning

import gridfs
import os
import sys

from scrapers.sreality.flat.images_scraper import SrealityImagesScraper



if __name__ == '__main__':

    learning = FlatImgQualityLearning()
#    learning.extract_images()
    model = learning.get_model()
    learning.learn_model(model)





