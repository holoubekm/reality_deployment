from common.utils import Stopwatch

from pymongo import MongoClient
from datetime import datetime

from models.flat_images_quality_learning import FlatPriceQualityLearning

import gridfs
import os
import sys

from scrapers.sreality.flat.images_scraper import SrealityImagesScraper



if __name__ == '__main__':

    sw = Stopwatch()

    learning = FlatPriceQualityLearning()
    with sw("loading model"):
        model = learning.load_model('./models/imgtype')

    filename  = './data_cache/imgquality/novostavba/e2e360311307e2ab6fd0ff6050eefe49ca3c92acac39b576b6db386e09ffba27e46ec48b567291b834241a3f6e4ac82e08bcf843ac1d6386145939eecd778ccf.jpeg'
    filename2 = './data_cache/imgquality/novostavba/e2e11d807ea88f6f447f1f539b092d21bd73f69f2ad94512f3cac8bbcb44c2a5c6ff4f39a3ab321bfc2d19fedf5b9af1f534b75111f2200c543e9b2f540521a2.jpeg'

    with sw("predicting model"):
        predicted = learning.predict(model, filename)
        print(predicted)





