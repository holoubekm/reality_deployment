
class FlatPriceQualityLearning(object):



    def __init__(self):
        self.img_size = (224,224)
        pass

    def data_to_pickle(self):

        from keras.applications.mobilenet import preprocess_input
        from keras.preprocessing.image import ImageDataGenerator

        train_datagen=ImageDataGenerator(preprocessing_function=preprocess_input) #included in our dependencies
        train_generator=train_datagen.flow_from_directory('./data_cache/imgtype/', # this is where you specify the path to the main data folder
                                                        target_size=self.img_size,
                                                        color_mode='rgb',
                                                        batch_size=32,
                                                        class_mode='categorical',
                                                        shuffle=True)
        print(train_generator)

    def predict(self, model, path):
        import cv2
        image = cv2.imread(path)
	    image = cv2.resize(image, self.img_size)
	    image = img_to_array(image)

        test_predictions = model.predict([image])

    def load_model(self, path):
        from keras.models import load_model
        return load_model(path)


    def learn_model(self, model):
        from keras.applications.mobilenet import preprocess_input
        from keras.preprocessing.image import ImageDataGenerator
        from keras.optimizers import Adam

#        return 1


#        for layer in model.layers[:20]:
#            layer.trainable=False
#        for layer in model.layers[20:]:
#            layer.trainable=True


        train_datagen=ImageDataGenerator(preprocessing_function=preprocess_input) #included in our dependencies
        train_generator=train_datagen.flow_from_directory('./data_cache/imgtype/', # this is where you specify the path to the main data folder
                                                        target_size=(224,224),
                                                        color_mode='rgb',
                                                        batch_size=32,
                                                        class_mode='categorical',
                                                        shuffle=True)

	

        model.compile(optimizer='Adam',loss='categorical_crossentropy',metrics=['accuracy'])
        # Adam optimizer
        # loss function will be categorical cross entropy
        # evaluation metric will be accuracy

        step_size_train=train_generator.n//train_generator.batch_size
        model.fit_generator(generator=train_generator,
                                steps_per_epoch=step_size_train,
                                epochs=10)
    
    def get_model(self):

        from keras.layers import Dense,GlobalAveragePooling2D
        from keras.applications import VGG16, VGG19
        from keras.applications import MobileNet
        from keras.preprocessing import image

        from keras.models import Model

        base_model=MobileNet(weights='imagenet',input_shape=(224,224,3),include_top=False) #imports the mobilenet model and discards the last 1000 neuron layer.

        x=base_model.output
        x=GlobalAveragePooling2D()(x)
        x=Dense(1024,activation='relu')(x) #we add dense layers so that the model can learn more complex functions and classify for better results.
        x=Dense(1024,activation='relu')(x) #dense layer 2
        x=Dense(512,activation='relu')(x) #dense layer 3
        preds=Dense(2,activation='softmax')(x) #final layer with softmax activation

        model=Model(inputs=base_model.input,outputs=preds)

        return model

