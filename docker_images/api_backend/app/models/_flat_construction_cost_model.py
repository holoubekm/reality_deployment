class FlatConstructionCostModelInference(object):
    __instance__ = None

    def get_estimation(self, params):

        ### average ceiling height even with floor thickness
        if "ceiling_height" in params:
            ceiling_height = params["ceiling_height"]
        else
            ceiling_height = 3


        ### built up area
        params["total_built_up_area"] * 3

        return 5*1000*1000
        
