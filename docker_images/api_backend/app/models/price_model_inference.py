import pandas as pd
import numpy as np
from json import load as json_load
from os import path
import bootstrapped.bootstrap as bs
import bootstrapped.stats_functions as bs_stats

from common.exceptions import MissingAPIFeaturesException, UnexpectedAPIFeaturesException
from common.normalizer import Normalizer
from providers import AggregatedProvider


class PriceModelInference(object):
    __flat_instance__ = None
    __house_instance__ = None

    def __init__(self, estate_type, model_name, models_dir):
        super().__init__()

        self.estate_type = estate_type
        self.models_dir = models_dir
        self.model_name = model_name

        self.keras_model_path = None
        self.keras_model_weights_path = None

        self.keras_model = None
        self.model_details = None

        self.feature_mapping = None

        self.normalizer_feature_subset = None

        self.normalizer = None

        self.features_importance = None
        self.api_features_importance = None

        self.feature_subset = None
        self.all_learning_features = None
        self.all_learning_features_details = None
        self.api_features = None
        self.api_features_details = None

        self.load_model_data()
        
        self.aggregated_provider = AggregatedProvider()


    def get_flat_instance():
        if PriceModelInference.__flat_instance__ == None:
            # PriceModelInference.__flat_instance__ = PriceModelInference('flat', model_name='flat_price_model_2019-06-23_12-00-02_mae_epochs=250_bs=64_fc=797_lr=0.0005_drop=0.25', models_dir='/models/saved')
            PriceModelInference.__flat_instance__ = PriceModelInference('flat', model_name='flat_price_model_2019-08-12_21-58-55_mean_absolute_percentage_error_epochs=250_bs=64_fc=161_lr=0.0005_drop=0.25', models_dir='app/models/saved')
        return PriceModelInference.__flat_instance__


    def get_house_instance():
        if PriceModelInference.__house_instance__ == None:
            PriceModelInference.__house_instance__ = PriceModelInference('house', model_name='house_price_model_2019-08-12_22-38-00_mean_absolute_percentage_error_epochs=250_bs=64_fc=201_lr=0.0005_drop=0.25', models_dir='app/models/saved')
            # PriceModelInference.__house_instance__ = PriceModelInference('house', model_name='__set_this_please__', models_dir='models/saved')
        return PriceModelInference.__house_instance__


    def get_inference_data(self, lon, lat, description, external_api_data, sanitize_api_data=True):
        if sanitize_api_data:
            sanitized_api_data = self.sanitize_api_data(external_api_data)
            del external_api_data
        else:
            sanitized_api_data = external_api_data

        inference_data = self.aggregated_provider.fetch_additional_data(lon, lat, description)
        inference_data.update(sanitized_api_data)
        del sanitized_api_data
        return inference_data

    def load_model_data(self):
        assert self.keras_model is None, f'The keras model has already been loaded'

        model_dir = path.join(self.models_dir, self.model_name)
        self.keras_model_path = path.join(model_dir, 'keras_model.json')
        self.keras_model_weights_path = path.join(model_dir, 'keras_model_weights.h5')
        model_details_path = path.join(model_dir, 'model_details.json')

        assert path.exists(self.keras_model_path), f'Can not load the inferencing model. Missing file: [{self.keras_model_path}]'
        assert path.exists(self.keras_model_weights_path), f'Can not load the model weight. Missing file: [{self.keras_model_weights_path}]'
        assert path.exists(model_details_path), f'Can not load the model details. Missing file: [{model_details_path}]'

        # with self.sw(f'Loading the keras model: [{self.model_path}]'):
        # from keras.models import load_model
        # self.keras_model = load_model(self.model_path)

        # from keras.backend import set_session
        # import tensorflow as tf
        # proto_config = tf.ConfigProto()
        # proto_config.gpu_options.per_process_gpu_memory_fraction = 0.1
        # set_session(tf.Session(config=proto_config))
        # TODO implement the memory limitation
        # TODO implement inferencing using a standalone docker
        
        with open(model_details_path, 'r', encoding='utf-8') as inp_file:
            self.model_details = json_load(inp_file)

        self.feature_mapping = self.model_details['feature_mapping']
        self.api_features = self.model_details['api_features']
        self.normalizer_feature_subset = self.model_details['normalizer_feature_subset']

        normalizer_config = self.model_details['normalizer_config']
        self.normalizer = Normalizer(config=normalizer_config)


        self.features_importance = {}
        self.api_features_importance = {}

        base_loss = self.model_details['feature_weights']['base_loss']['absolute_loss']['mean']
        feature_weights = self.model_details['feature_weights']['loss_without_feature']
        for feature_id, value in feature_weights.items():
            loss_gain = value['absolute_loss']['mean'] - base_loss
            self.features_importance[feature_id] = loss_gain
            if feature_id in self.api_features:
                self.api_features_importance[feature_id] = loss_gain


        # self.feature_subset = self.model_details['feature_subset']
        # self.all_learning_features = normalizer_config['features']
        # self.all_learning_features_details = {feature['feature_id'] for feature in self.all_learning_features}
        # self.api_features_details = [feature for feature in self.all_learning_features if feature['feature_id'] in self.api_features]

        self.feature_subset = self.model_details['feature_subset']
        all_dataset_features = normalizer_config['features']
        all_learning_features = self.model_details['feature_weights']['loss_without_feature']
        self.all_learning_features_details = {feature['feature_id']: feature for feature in all_dataset_features if feature['feature_id'] in all_learning_features}
        # Only features used during the learning!
        self.all_learning_features = sorted(list(self.all_learning_features_details.keys()))
        # Only features used during the learning!
        self.api_features_details = {feature_id: feature for feature_id, feature in self.all_learning_features_details.items() if feature_id in self.api_features}


    def load_keras_model(self):
        from keras.models import model_from_json

        # from keras.backend import set_session
        # import tensorflow as tf
        # proto_config = tf.ConfigProto()
        # proto_config.gpu_options.per_process_gpu_memory_fraction = 0.1
        # set_session(tf.Session(config=proto_config))
        
        with open(self.keras_model_path, 'r', encoding='utf-8') as inp_file:
            self.keras_model = model_from_json(inp_file.read())
        self.keras_model.load_weights(self.keras_model_weights_path)


    def sanitize_api_data(self, external_api_data):
        external_api_data_ids = set(external_api_data.keys())
        
        features_missing = set(self.api_features).difference(external_api_data_ids)
        if len(features_missing) > 0:
            raise MissingAPIFeaturesException(f'Some features are missing in the request: [{features_missing}]')

        features_unexpected = external_api_data_ids.difference(self.api_features)
        if len(features_unexpected) > 0:
            raise UnexpectedAPIFeaturesException(f'Unexpected features in the request: [{features_unexpected}]')

        return external_api_data

    def get_all_features(self, with_details=False):
        if with_details:
            return self.all_learning_features_details
        return self.all_learning_features

    def get_api_features(self):
        return self.api_features_details

    def get_all_features_importance(self):
        return self.features_importance

    def get_api_features_importance(self):
        return self.api_features_importance

    def get_testing_error_statistics(self):
        absolute_loss = self.model_details['testing']['statistics']['absolute_loss']
        return absolute_loss['statistics']

    def get_inference_error_estimation(self, predicted_scalar):
        absolute_loss = self.model_details['testing']['statistics']['absolute_loss']['statistics']
        bins = absolute_loss['bin_ranges']
        means = absolute_loss['mean']
        stds = absolute_loss['std']
        counts = absolute_loss['count']

        mean, std, count = None, None, None
        for i in range(0, len(bins)-1):
            if bins[i] <= predicted_scalar <= bins[i+1]:
                mean, std, count = means[i], stds[i], counts[i]
                break


        test_true = self.model_details['testing']['data']['reality']
        test_pred = self.model_details['testing']['data']['prediction']

        df = pd.DataFrame({'true': test_true, 'pred': test_pred})
        df['err'] = df['pred'] - df['true']
        df['dff'] = abs(df['pred'] - predicted_scalar)
        errs_sample = df.sort_values(by='dff')[:50]

        intervals = []
        for alpha in [0.05, 0.1, 0.2]:
            res = bs.bootstrap(errs_sample['err'].values, bs_stats.mean, num_iterations=10000, alpha=alpha)
            intervals.append({
                'alpha': float(alpha),
                'lower_bound': float(res.lower_bound),
                'value': float(res.value),
                'upper_bound': float(res.upper_bound)
            })

        # point = {
        #     'mean': mean,
        #     'std': std,
        #     'count': count,
        #     'metric': 'absolute_loss'
        # }

        return {'intervals': intervals}

    def get_estimation(self, lon, lat, external_api_data, with_error_estimation=False, description=None, sanitize_api_data=True):
      
        inference_data = self.get_inference_data(lon, lat, description, external_api_data, sanitize_api_data=sanitize_api_data)
        # import sys
        # print(inference_data, file=sys.stderr)
        inference_data = {a:inference_data[a] for a in inference_data if a in self.all_learning_features_details}

        self.normalizer.reset()
        self.normalizer.add_row(inference_data)
        del inference_data
        
        normalized_bucket = self.normalizer.normalize()
        normalized_data = normalized_bucket['data']
        df = pd.DataFrame(normalized_data)
        del normalized_data

        df = df[self.normalizer_feature_subset]

        assert len(df) == 1, f'Exactly one row should be in the data table for single flat price prediction.'
        
        if not self.keras_model:
            self.load_keras_model()

        predicted_raw = self.keras_model.predict(df.values)
        predicted_scalar = float(np.asscalar(predicted_raw))
        del df

        # TODO log results here man!

        result = {
            'prediction': predicted_scalar
        }

        if with_error_estimation:
            result['error_estimation'] = self.get_inference_error_estimation(predicted_scalar)

        return result

