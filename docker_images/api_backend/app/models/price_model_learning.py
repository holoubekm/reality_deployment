from datetime import datetime
import numpy as np
from sklearn.model_selection import train_test_split
from os import path, makedirs
from collections import defaultdict
from json import dump as json_dump, JSONEncoder
from numpy.random import normal
from numpy import ma

from common.exceptions import InternalException
from common.utils import Stopwatch, load_config, text_lower_rm_accents


class NumpyEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)


class PriceModelLearning(object):

    def __init__(self, estate_type, config_path, normalizer_config, feature_mapping):
        super().__init__()
        self.config = load_config(config_path, config_dir='configs')
        self.normalizer_config = normalizer_config
        self.feature_mapping = feature_mapping
        self.estate_type = estate_type

        self.feature_subset = self.config['feature_subset']

        self.normalizer_feature_subset = []
        for feature in self.feature_subset:
            for normalizer_feature in feature_mapping[feature]:
                assert normalizer_feature not in self.normalizer_feature_subset, f'There is some inconsistency, the same normalizer feature belongs to two different features: [{normalizer_feature}]'
                self.normalizer_feature_subset.append(normalizer_feature)

        self.sw = Stopwatch()
        self.num_confidence_bins = 10

        self.lower_price_limit =  self.config['lower_price_limit']
        self.upper_price_limit = self.config['upper_price_limit']
        self.row_count_limit = 20000
        self.test_size_ratio = 0.2

        self.epochs = 250
        self.batch_size = 64
        self.learning_rate = 0.0005
        self.drop = 0.25
        self.learning_loss = 'mean_absolute_percentage_error'

        self.model = None
        self.model_code = None
        self.model_name = None
        self.row_count = None
        self.feature_count = None
        self.data_frame = None
        self.x_train, self.x_test, self.y_train, self.y_test = None, None, None, None

    def get_log_dir(self):
        model_name_norm = text_lower_rm_accents(self.model_name)
        return f'logs/tensor_board/prices/{model_name_norm}/'

    def get_keras_model(self):
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session
        from keras.layers import Dense, Dropout, Activation
        from keras.models import Sequential
        from keras.optimizers import Adam
        from keras.layers.normalization import BatchNormalization

        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.60
        set_session(tf.Session(config=config))

        with self.sw('Generating and compiling the model'):
            model_code = '''
                global model
                model = Sequential()
                model.add(Dense(8192, input_dim=self.feature_count, kernel_initializer='normal', activation='relu'))
                model.add(Dropout(self.drop))
                model.add(Dense(4096, kernel_initializer='normal', activation='relu'))
                model.add(Dropout(self.drop))
                model.add(Dense(512, kernel_initializer='normal', activation='relu'))
                model.add(Dropout(self.drop))
                model.add(Dense(1, kernel_initializer='normal', activation='linear'))
                optimizer = Adam(lr=self.learning_rate)
                model.compile(loss=self.learning_loss, optimizer=optimizer, metrics=['mse', 'mae', 'mean_absolute_percentage_error', 'mean_squared_logarithmic_error'])
            '''

            global model
            exec('\n'.join([line.strip() for line in model_code.splitlines()]))
            return model, model_code


    def set_learning_data(self, data_frame):
        with self.sw('Filtering and preprocessing data'):
            if self.row_count_limit > len(data_frame):
                print(f'Notice: learning data: [{len(data_frame)}] rows will be cropped to [{self.row_count_limit}] rows')

            data_frame = data_frame[:self.row_count_limit]
            data_frame = data_frame.where((data_frame['price'] >= self.lower_price_limit) & (data_frame['price'] <= self.upper_price_limit)).dropna(subset=['price'])
            print(f'Notice: total learning data length: [{len(data_frame)}]')

            self.data_frame = data_frame
            X_all = data_frame[self.normalizer_feature_subset].values
            Y_all = data_frame['price'].values

            self.row_count = X_all.shape[0]
            self.feature_count = X_all.shape[1]

            self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(X_all, Y_all, test_size=self.test_size_ratio, random_state=0, shuffle=True)
            
            del X_all
            del Y_all

            now = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            self.model_name = f'{self.estate_type}_price_model_{now}_{self.learning_loss}_epochs={self.epochs}_bs={self.batch_size}_fc={self.feature_count}_lr={self.learning_rate}_drop={self.drop}'

    def start_learning(self):
        if self.x_train is None or self.y_train is None:
            raise InternalException('You are supposed to call the [set_learning_data] method prior to start learning.')

        self.model, self.model_code = self.get_keras_model()
        
        # TODO tensorboard on demand - add to the config
        from keras.callbacks import TensorBoard
        tensorboard = TensorBoard(log_dir=self.get_log_dir(), write_graph=True, write_images=True)
        # self.model.fit(self.x_train, self.y_train, epochs=self.epochs, batch_size=self.batch_size, callbacks=[tensorboard], validation_data=(self.x_test, self.y_test), shuffle=True, verbose=1)
        self.model.fit(self.x_train, self.y_train, epochs=self.epochs, batch_size=self.batch_size, callbacks=[tensorboard], validation_data=(self.x_test, self.y_test), shuffle=True, verbose=1)

    def save_model(self, models_dir):
        if self.model is None:
            raise InternalException('You are supposed to call the [start_learning] method prior to save the model.')

        model_dir = path.join(models_dir, self.model_name)
        keras_model_path = path.join(model_dir, 'keras_model.json')
        keras_model_weights_path = path.join(model_dir, 'keras_model_weights.h5')
        model_details_path = path.join(model_dir, 'model_details.json')

        if not path.exists(model_dir):
            makedirs(model_dir)

        with self.sw(f'Saving the keras model: [{keras_model_path}]'):
            model_json = self.model.to_json()
            with open(keras_model_path, 'w', encoding='utf-8') as out_file:
                out_file.write(model_json)
            self.model.save_weights(keras_model_weights_path)
            # self.model.save(keras_model_path)

        with self.sw(f'Computing model statistics'):
            train_data_stats, train_pred = self.evaluate_model(self.x_train, self.y_train)
            test_data_stats, test_pred = self.evaluate_model(self.x_test, self.y_test)

        with self.sw(f'Computing feature weights using the change-of-loss metrics'):
            feature_weights = self.evaluate_features()

        results = {
            'model_name': self.model_name,
            'row_count_limit': self.row_count_limit,
            'row_count': self.row_count,
            'feature_count': self.feature_count,
            'test_size_ratio': self.test_size_ratio,
            'lower_price_limit': self.lower_price_limit,
            'upper_price_limit': self.upper_price_limit,
            'num_confidence_bins': self.num_confidence_bins,
            'learning_loss': self.learning_loss,
            'learning_rate': self.learning_rate,
            'epochs': self.epochs,
            'batch_size': self.batch_size,
            'drop': self.drop,
            'model_code': self.model_code,
            'training': {
                'row_count': len(self.x_train),
                'statistics': train_data_stats,
                'data': {
                    'reality': self.y_train.tolist(),
                    'prediction': train_pred.tolist()
                }
            },
            'testing': {
                'row_count': len(self.x_test),
                'statistics': test_data_stats,
                'data': {
                    'reality': self.y_test.tolist(),
                    'prediction': test_pred.tolist()
                }
            },
            'feature_mapping': self.feature_mapping,
            'feature_subset': self.feature_subset,
            'normalizer_feature_subset': self.normalizer_feature_subset,
            'feature_weights': feature_weights,
            'normalizer_config': self.normalizer_config
        }

        with open(model_details_path, 'w', encoding='utf-8') as out_file:
            json_dump(results, out_file, cls=NumpyEncoder)
        return results

    @staticmethod
    def loss_abs_fn(y_pred, y_true):
        return np.abs(y_pred - y_true)

    @staticmethod
    def loss_pct_fn(y_pred, y_true):
        return (100 * (y_pred - y_true)) / y_true

    @staticmethod
    def loss_pct_abs_fn(y_pred, y_true):
        return np.abs(((100 * (y_pred - y_true)) / y_true))

    def evaluate_model(self, x_vals, y_true):
        evaluation = {}
        y_pred = np.clip(self.model.predict(x_vals).flatten().astype(np.float64), self.lower_price_limit, self.upper_price_limit)

        loss_defs = [('absolute_loss', self.loss_abs_fn)]
        for loss_name, loss_fn in loss_defs:
            loss_values = loss_fn(y_pred, y_true)
            evaluation[loss_name] = {
                'mean_loss': loss_values.mean(),
                'std_loss': loss_values.std(),
                'min_loss': loss_values.min(),
                'max_loss': loss_values.max()
            }

        num_confidence_bins = self.num_confidence_bins
        for loss_name, loss_fn in loss_defs:
            loss_values = loss_fn(y_pred, y_true).tolist()

            bin_ranges = np.histogram(y_pred, bins=num_confidence_bins, density=False)[1].tolist()
            # bin_ranges = list(np.linspace(min(y_true), max(y_true), num_confidence_bins, endpoint=False))

            y_pred_to_bins = np.digitize(y_pred, bin_ranges) - 1
            y_pred_bins = defaultdict(list)
            
            # TODO check this statement in the generated stats!!!!
            y_pred_to_bins[len(y_pred_to_bins) - 1] = num_confidence_bins

            for i, loss_value in enumerate(loss_values):
                cur_bin = y_pred_to_bins[i]
                if cur_bin == num_confidence_bins:
                    cur_bin = y_pred_to_bins[i] - 1
                y_pred_bins[cur_bin].append(loss_value)

            stats = {'bin_ranges': bin_ranges, 'bin_count': num_confidence_bins, 'mean': [], 'std': [], 'count': []}
            for bin_index in range(0, num_confidence_bins):
                prediction_losses = y_pred_bins[bin_index]
                if len(prediction_losses):
                    stats['mean'].append(np.mean(prediction_losses))
                    stats['std'].append(np.std(prediction_losses))
                    stats['count'].append(len(prediction_losses))
                else:
                    stats['mean'].append(None)
                    stats['std'].append(None)
                    stats['count'].append(0)

            evaluation[loss_name]['statistics'] = stats
        return evaluation, y_pred

    def get_feature_loss(self, y_pred, y_true):
        # loss_defs = [('absolute_loss', self.loss_abs_fn), ('percent_loss', self.loss_pct_fn), ('percent_loss_abs', self.loss_pct_abs_fn)]
        loss_defs = [('absolute_loss', self.loss_abs_fn)]
        results = {}
        for loss_name, loss_fn in loss_defs:
            loss_values = loss_fn(y_pred, y_true)
            loss_values = ma.masked_invalid(loss_values)
            results[loss_name] = {
                'mean': np.mean(loss_values),
                'std': np.std(loss_values),
                'min': np.min(loss_values),
                'max': np.max(loss_values),
            }
        return results

    def evaluate_features(self):
        x_all = self.data_frame[self.normalizer_feature_subset].values
        y_true = self.data_frame['price'].values

        y_pred = self.model.predict(x_all).flatten().astype(np.float64)
        base_loss = self.get_feature_loss(y_pred, y_true)

        loss_without_feature = {}
        for i, feature in enumerate(self.feature_subset):
            data_frame = self.data_frame.copy()

            for normalizer_feature in self.feature_mapping[feature]:
                
                ## col = data_frame[normalizer_feature].values
                ## mean = np.mean(col)
                ## std = np.std(col)
                ## data_frame[normalizer_feature] = normal(mean, std, len(col))

                # Not all features are normally distributed - use shuffling instead!
                data_frame[normalizer_feature] = np.random.permutation(data_frame[normalizer_feature])

            x_test = data_frame[self.normalizer_feature_subset].values
            del data_frame

            y_pred = self.model.predict(x_test).flatten().astype(np.float64)
            del x_test
            current_loss = self.get_feature_loss(y_pred, y_true)
            loss_without_feature[feature] = current_loss

        return {'base_loss': base_loss, 'loss_without_feature': loss_without_feature}
