import psycopg2
import unicodedata
import re
from os import path
from pymongo import MongoClient
from time import time as tm
from datetime import datetime
# from secrets import token_hex
from json import load as json_load
# import hashlib


# from inspect import currentframe, getframeinfo
# class PerfCounter(object):
#     def __init__(self):
#         self.events = []

#     def __call__(self):
#         return self

#     def __enter__(self):
#         self.time = tm()

#     def __exit__(self, *args):
#         d = tm() - self.time
#         frameinfo = getframeinfo(currentframe())
#         self.events.append(frameinfo)

#     def to_list(self):
#         return self.events
        

class Stopwatch(object):
    __instance__ = None

    def __init__(self, _stop=False):
        if self.__instance__ is None and _stop:
            self.__instance__ = Stopwatch(True)
            return self.__instance__

        self.time = None
        self.msg = ''

    def __call__(self, msg):
        self.msg = msg
        return self

    def __enter__(self):
        print(f'---> I\'m: {self.msg}')
        self.time = tm()
        return self

    def __exit__(self, *args):
        d = tm() - self.time
        print(f'---> {self.msg} took: [{d:0.2f}]s\n')


def text_lower_rm_accents(text):
    text = text.lower()
    text = unicodedata.normalize('NFKD', text).encode('ASCII', 'ignore').decode('ASCII')
    return text


def text_lower_rm_accents_rm_ws(text):
    text = text.lower()
    text = unicodedata.normalize('NFKD', text).encode('ASCII', 'ignore').decode('ASCII')
    return re.sub(r'[^\w]+', '_', text)


# def rgb_to_hex(rgb):
#     return '#' + ''.join(list(hex(rgb[i])[2:] for i in (0, 1, 2)))


# def lerp_color(alpha, min_alpha, max_alpha, end=(231, 76, 60), start=(46, 204, 113)):
#     alpha = (alpha - min_alpha) / (max_alpha - min_alpha)
#     if alpha < 0:
#         return rgb_to_hex(end)
#     elif alpha > 1:
#         return rgb_to_hex(start)

#     r = int(alpha * start[0] + (1 - alpha) * end[0])
#     g = int(alpha * start[1] + (1 - alpha) * end[1])
#     b = int(alpha * start[2] + (1 - alpha) * end[2])
#     return rgb_to_hex((r, g, b))


def get_czech_statistical_office_connection():
    return MongoClient('mongodb://data_cso:27017/')
    
def get_surroundings_cache_connection():
    return MongoClient('mongodb://data_cso:27017/')

def get_scraper_cache_connection():
    return MongoClient('mongodb://data_cso:27017/')
    
def get_postgis_floods_connection():
    return psycopg2.connect("dbname='pgsnapshot' user='postgres' host='data_surroundings_and_floods' port='5432' password='postgres'")

def get_postgis_surroundings_connection():
    return psycopg2.connect("dbname='pgsnapshot' user='postgres' host='data_surroundings_and_floods' port='5432' password='postgres'")
    
def get_reverse_geocoding_connection():
    return 'http://data_reverse_geocoder:8080/reverse.php'

# def add_value(dictionary, key, value):
#     if key not in dictionary:
#         dictionary[key] = []
#     dictionary[key].append(value)


# def add_unique_value(dictionary, key, value):
#     if key not in dictionary:
#         dictionary[key] = set()
#     dictionary[key].add(value)


# def date_time_day_formatted():
#     return datetime.now().strftime("%Y-%m-%d")


def date_time_formatted():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")


# def hash_column(col_name, col_type=None):
#     col_hex = token_hex(5)
#     if col_type:
#         return f'{col_name}_{col_type[:3]}_{col_hex}'
#     return f'{col_name}_{col_hex}'


# def load_config(config_id, file_name, config_dir='configs'):
#     config_path = path.join(config_dir, config_id, file_name)
#     with open(config_path, 'r', encoding='utf-8') as inp_file:
#         config = json_load(inp_file)
#         # TODO remove config['id'] at all
#         # assert config['id'] == config_id, f'The config ID passed as parament does not match ID inside the config: [{config_id}] x [{config["id"]}]'
#         return config

def load_config(file_name, config_dir):
    config_path = path.join(config_dir, file_name)
    with open(config_path, 'r', encoding='utf-8') as inp_file:
        return json_load(inp_file)


# def hash_iterable_bytes(iterable):
#     h = hashlib.md5()
#     for item in iterable:
#         h.update(item)
#     return h.hexdigest()


# def hash_bytes(binary):
#     h = hashlib.md5()
#     h.update(binary)
#     return h.hexdigest()


# def hash_string(string):
#     h = hashlib.md5()
#     h.update(string.encode())
#     return h.hexdigest()


# def hash_config(config_id, file_name, config_dir='configs'):
#     config_path = path.join(config_dir, config_id, file_name)
#     h = hashlib.md5()
#     with open(config_path, 'rb', buffering=0) as f:
#         for b in iter(lambda: f.read(128 * 1024), b''):
#             h.update(b)
#     return h.hexdigest()


def parse_int(arg):
    try:
        val = str(arg)
        val = re.sub(r'[^0-9]', '', val)
        return int(val)
    except ValueError:
        return None


def parse_float(arg):
    try:
        val = str(arg).replace(',', '.')
        val = re.sub(r'[^0-9.]', '', val)
        return float(val)
    except ValueError as msg:
        print(msg)
        return None


# def assert_nom_bool(value):
#     assert value in ['true', 'false'], f'Unexpected bool value: [{value}], expected: ["true", "false"]'


# def create_mongo_collection_if_not_exists(database, collection_name):
#     if collection_name not in database.list_collection_names():
#         database.create_collection(collection_name, storageEngine={'wiredTiger': {'configString': 'block_compressor=zlib'}})
#         return True
#     return False



def get_obj_size(obj):
    import gc
    import sys

    marked = {id(obj)}
    obj_q = [obj]
    sz = 0

    while obj_q:
        sz += sum(map(sys.getsizeof, obj_q))

        # Lookup all the object reffered to by the object in obj_q.
        # See: https://docs.python.org/3.7/library/gc.html#gc.get_referents
        all_refr = ((id(o), o) for o in gc.get_referents(*obj_q))

        # Filter object that are already marked.
        # Using dict notation will prevent repeated objects.
        new_refr = {o_id: o for o_id, o in all_refr if o_id not in marked and not isinstance(o, type)}

        # The new obj_q will be the ones that were not marked,
        # and we will update marked with their ids so we will
        # not traverse them again.
        obj_q = new_refr.values()
        marked.update(new_refr.keys())

    return sz