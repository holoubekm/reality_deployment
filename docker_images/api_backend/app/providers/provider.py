# from common.utils import load_config, hash_config


class Provider(object):
    def __init__(self) -> None:
        super().__init__()
        # self.config_id = config_id
        # self.config = load_config(config_id, config_name)
        # self.name = self.config['name']
        # self.pipeline_id = ''
        # if hash_config_as_pipeline_id:
            # self.pipeline_id = hash_config(config_id, config_name)

    def load_from_cache(self, *args, **kwargs):
        raise NotImplementedError('This method should be overridden!')
