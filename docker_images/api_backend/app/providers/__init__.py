from app.providers.floods import FloodsProvider
from app.providers.surroundings import SurroundingsProvider
from app.providers.reverse_geocoding import ReverseGeocodingProvider
from app.providers.czech_statistical_office import CzechStatisticalOfficeProvider
from app.providers.description import DescriptionProvider
from app.providers.aggregated import AggregatedProvider
# from app.providers.scrapers import *

# from app.providers.scrapers import SrealityScraper
from app.providers.scrapers import SrealityDetailsScraper
from app.providers.scrapers import SrealityFeaturesMiner