import requests
from app.common.utils import get_reverse_geocoding_connection
from app.providers.provider import Provider
from json.decoder import JSONDecodeError
from app.common.exceptions import LocationOutOfTheCountryException


class ReverseGeocodingProvider(Provider):
    # __instance__ = None

    def __init__(self) -> None:
        super().__init__()
    #     self.collection_cache_name = f'nominatim_items_{self.pipeline_id}'

    # def get_cache_collection(self):
    #     with get_connection() as client:
    #         database = client['cache']
    #         return database[self.collection_cache_name]

    # def get_instance():
        # if ReverseGeocodingProvider.__instance__ == None:
            # ReverseGeocodingProvider.__instance__ = ReverseGeocodingProvider()
        # return ReverseGeocodingProvider.__instance__

    def get_address(self, lon, lat):
        params = {
            'format': 'json',
            'lat': lat,
            'lon': lon,
            'zoom': 18
        }
        url = get_reverse_geocoding_connection()
        resp = requests.get(url, params=params)
        data = resp.json()

        if 'error' in data:
            raise LocationOutOfTheCountryException(f'Location [{lon}, {lat}] is out of the country')
        return data['address']

    # def cache_address(self, lon, lat, address):
    #     item = {
    #         "lat": lat,
    #         "lon": lon,
    #         "config_id": self.config_id,
    #         "address": address
    #     }
    #     collection = self.get_cache_collection()
    #     is_duplicate = {"lat": lat, "lon": lon}
    #     written = collection.update_one(is_duplicate, {"$setOnInsert": item}, upsert=True)
    #     if written.upserted_id:
    #         print(f'Cached nominatim item with an id: [{written.upserted_id}]')

    def load_from_cache(self, lon, lat):
        # collection = self.get_cache_collection()
        # cached = collection.find_one({"lat": lat, "lon": lon})
        # if cached:
            # return cached['address']

        address = self.get_address(lon, lat)
        # if address:
            # self.cache_address(lon, lat, address)
        return address
