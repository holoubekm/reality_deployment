from app.common.utils import Stopwatch, get_surroundings_cache_connection, get_postgis_surroundings_connection, load_config
from app.providers.provider import Provider
from math import pi as math_pi
import numpy as np


class SurroundingsProvider(Provider):
    # __instance__ = None

    def __init__(self) -> None:
        super().__init__()
        self.conn = get_postgis_surroundings_connection()
        self.config = load_config('surroundings.json', config_dir='app/providers/configs')
        self.distances = self.config['distances']
        self.max_distance = max(self.distances)
        self.collection_cache_name = 'surroundings'
        
        self.group_names = {group['name'] for group in self.config['groups']}
        self.column_names = self.get_column_names()

    # def get_instance():
        # if SurroundingsProvider.__instance__ == None:
            # SurroundingsProvider.__instance__ = SurroundingsProvider()
        # return SurroundingsProvider.__instance__


    @staticmethod
    def get_col_name(group_name, metrics, dist_limit):
        return f'surroundings_{group_name}_{metrics}_{dist_limit}_ord_float'


    def get_surroundings_types(self):
        return self.config['groups']


    def get_column_names(self):
        column_names = set()
        for distance in self.distances:
            for group_name in self.group_names:
                count_name = self.get_col_name(group_name, 'count', distance)
                assert count_name not in column_names, f'The is some inconsistency in the config file - duplicate column found: [{count_name}]'
                column_names.add(count_name)
                density_name = self.get_col_name(group_name, 'density', distance)
                assert density_name not in column_names, f'The is some inconsistency in the config file - duplicate column found: [{density_name}]'
                column_names.add(density_name)
        return column_names

    # def get_final_query(self):
    #     queries = []
    #     for distance in self.distances:
    #         for group_name in self.group_names:
    #             column_name = self.get_col_name(group_name, 'count', distance)
    #             query = f"SELECT '{column_name}' AS name, SUM({group_name}) AS count FROM tmp"
    #             queries.append(query)
    #     return '\n\tUNION ALL\n\t'.join(queries)

    def get_query(self, lon, lat):
        columns_query = ', '.join(self.group_names)
        # final_query = self.get_final_query()
        query = f"""
                SELECT 
                    ST_Distance(f.centroid, ST_MakePoint({lon}, {lat}), true) as distance,
                    -- f.src as source,
                    -- f.tags->'name' as name,
                    {columns_query}
                    FROM all_filtered f WHERE ST_DWithin(ST_MakePoint({lon}, {lat}), f.centroid, {self.max_distance}, true);
                """
        return query

    def load_from_sql(self, lon, lat):
        query = self.get_query(lon, lat)

        data = {}
        not_added_columns = set(self.column_names)
        with self.conn.cursor() as cursor:
            cursor.execute(query)
            ret = cursor.fetchall()
            mapping = {col.name:i for i, col in enumerate(cursor.description)}

            df = np.asarray(ret, dtype=np.int64)
            
            for distance in self.distances:
                filtered = df[np.where(df[:, 0] <= distance)]
                counts = np.sum(filtered, axis=0)

                for column in self.group_names:
                    index = mapping[column]
                    count = counts[index]
                    col_count_name = self.get_col_name(column, 'count', distance)
                    col_density_name = self.get_col_name(column, 'density', distance)
                    density = (1000000 * count) / (math_pi * distance * distance)
                    assert col_count_name not in data, f'Duplicate column found: [{col_count_name}] in data'
                    assert col_density_name not in data, f'Duplicate column found: [{col_density_name}] in data'
                    data[col_count_name] = float(count)
                    data[col_density_name] = float(density)
                    not_added_columns.remove(col_count_name)
                    not_added_columns.remove(col_density_name)

        for group_key in not_added_columns:
            data[group_key] = 0
        return data

    def get_cache_collection(self):
        with get_surroundings_cache_connection() as client:
            database = client['cache']
            return database[self.collection_cache_name]

    def cache_result(self, lon, lat, data, collection):
        item = {
            "lat": lat,
            "lon": lon,
            "data": data
        }
        is_duplicate = {"lat": lat, "lon": lon}
        written = collection.update_one(is_duplicate, {"$setOnInsert": item}, upsert=True)
        if written.upserted_id:
            print(f'Cached osm_data with an id: [{written.upserted_id}]')

    def load_from_cache(self, lon, lat):
        collection = self.get_cache_collection()
        cached = collection.find_one({"lat": lat, "lon": lon})
        if cached:
            return cached['data']

        sw = Stopwatch()
        with sw('Loading data from the PostgreSQL surroundings origin'):
            data = self.load_from_sql(lon, lat)
            if data:
                self.cache_result(lon, lat, data, collection)
            return data
