from app.providers import FloodsProvider, SurroundingsProvider, ReverseGeocodingProvider, CzechStatisticalOfficeProvider, DescriptionProvider
from app.common.exceptions import LocationOutOfTheCountryException

class AggregatedProvider(object):
    
    def __init__(self):
        super().__init__()
        self.reverse_geocoding_provider = ReverseGeocodingProvider()
        self.floods_provider = FloodsProvider()
        self.surroundings_provider = SurroundingsProvider()
        self.cso_provider = CzechStatisticalOfficeProvider()
        self.description_provider = DescriptionProvider()

    def get_area_flood_risk(self, lon, lat):
        flood_risk = self.floods_provider.load_from_cache(lon=lon, lat=lat)
        return {
            'has_5_years_flood_risk': flood_risk['flood_5_years_nom_boolean'],
            'has_20_years_flood_risk': flood_risk['flood_20_years_nom_boolean'],
            'has_100_years_flood_risk': flood_risk['flood_100_years_nom_boolean'],
            'is_active_flood_area': flood_risk['flood_active_area_nom_boolean']
        }

    def get_surroundings_types(self):
        return self.surroundings_provider.get_surroundings_types()

    def test_location_in_the_country(self, lon, lat):
        address_data = self.reverse_geocoding_provider.load_from_cache(lon=lon, lat=lat)
        if not address_data or 'county' not in address_data:
            # TODO - Make this error generic
            raise LocationOutOfTheCountryException(f'Location [{lon}, {lat}] is out of the country')

    def get_surroundings(self, lon, lat):
        return self.surroundings_provider.load_from_cache(lon=lon, lat=lat)

    def fetch_additional_data(self, lon, lat, description):
        data = {}
        address_data = self.reverse_geocoding_provider.load_from_cache(lon=lon, lat=lat)
        if not address_data or 'county' not in address_data:
            # TODO - Make this error generic
            raise LocationOutOfTheCountryException(f'Location [{lon}, {lat}] is out of the country')
        county = address_data['county']

        cso_data = self.cso_provider.load_from_cache(county=address_data['county'])
        data.update(cso_data)
        del cso_data

        floods_data = self.floods_provider.load_from_cache(lon=lon, lat=lat)
        data.update(floods_data)
        del floods_data

        surroundings_data = self.surroundings_provider.load_from_cache(lon=lon, lat=lat)
        data.update(surroundings_data)
        del surroundings_data

        if description:
            description_data = self.description_provider.load_from_cache(description=description)
            data.update(description_data)
            del description_data

        return data
