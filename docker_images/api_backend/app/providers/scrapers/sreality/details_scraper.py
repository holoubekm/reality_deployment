
from uuid import uuid4
from json import loads as json_loads
from time import sleep

from app.providers.scrapers import SrealityScraper

class SrealityDetailsScraper(SrealityScraper):
    def __init__(self):
        super().__init__()
        self.details_collection_name = f'online_details'

    def scrape(self, hash_id):
        with self.get_cache_connection() as connection:
            database = connection["scraped"]
            self.create_mongo_collection_if_not_exists(database, self.details_collection_name)
            collection = database[self.details_collection_name]

            print(f'Saving data to [{self.details_collection_name}]')

            found = collection.find_one({'hash_id': hash_id})
            if found:
                print(f'Skipping hash_id: [{hash_id}] - already exists')
                return found
            else:
                url = f'https://www.sreality.cz/api/cs/v2/estates/{hash_id}'
                data = self.make_get_request(url).json()
                item = {
                    "id": str(uuid4()),
                    'inserted_at': self.date_time_formatted(),
                    'data': data,
                    'hash_id': str(hash_id)
                }

                is_duplicate = {"hash_id": hash_id}
                written = collection.update_one(is_duplicate, {"$setOnInsert": item}, upsert=True)
                if written.upserted_id:
                    print(f'Saved hash_id: [{hash_id}]')
                return item
