from app.common.utils import get_scraper_cache_connection
import requests
from pymongo import MongoClient
from datetime import datetime

class SrealityScraper(object):
    def __init__(self) -> None:
        super().__init__()
        self.database_name = f'scraped'

    def get_cache_connection(self):
        return get_scraper_cache_connection()
        
    def create_mongo_collection_if_not_exists(self, database, collection_name, after_create = None):
        if collection_name not in database.list_collection_names():
            database.create_collection(collection_name, storageEngine={'wiredTiger': {'configString': 'block_compressor=zlib'}})
            if after_create is not None:
                after_create(database[collection_name])
            return True
        return False

    def scrape(self, *args, **kwargs):
        raise NotImplementedError('This method should be overridden!')

    def date_time_formatted(self):
        return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")

    def make_get_request(self, url):
        return requests.get(url)

