# from scrapers.sreality.flat.details_scraper import SrealityDetailsScraper as SrealityFlatDetailsScraper
# from scrapers.sreality.flat.stubs_scraper import SrealityStubsScraper as SrealityFlatStubsScraper
# from scrapers.sreality.flat.features_miner import SrealityFeaturesMiner as SrealityFlatFeaturesMiner
# from scrapers.sreality.flat.remote_features_fetcher import SrealityRemoteFeaturesFetcher as SrealityFlatRemoteFeaturesFetcher

# from scrapers.sreality.house.details_scraper import SrealityDetailsScraper as SrealityHouseDetailsScraper
# from scrapers.sreality.house.stubs_scraper import SrealityStubsScraper as SrealityHouseStubsScraper
# from scrapers.sreality.house.features_miner import SrealityFeaturesMiner as SrealityHouseFeaturesMiner
# from scrapers.sreality.house.remote_features_fetcher import SrealityRemoteFeaturesFetcher as SrealityHouseRemoteFeaturesFetcher

# from scrapers.scraper import SrealityScraper


from app.providers.scrapers.scraper import SrealityScraper
from app.providers.scrapers.sreality.details_scraper import SrealityDetailsScraper
from app.providers.scrapers.sreality.features_miner import SrealityFeaturesMiner
