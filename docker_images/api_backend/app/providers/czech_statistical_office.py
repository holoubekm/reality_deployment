from app.common.utils import get_czech_statistical_office_connection, Stopwatch, load_config
from app.providers.provider import Provider

class CzechStatisticalOfficeProvider(Provider):
    # __instance__ = None

    def __init__(self) -> None:
        super().__init__()
        self.config = load_config('czech_statistical_office.json', config_dir='app/providers/configs')
        self.counties = self.config['counties']
        self.cache = None

    # def get_instance():
    #     if CzechStatisticalOfficeProvider.__instance__ == None:
    #         CzechStatisticalOfficeProvider.__instance__ = CzechStatisticalOfficeProvider()
    #     return CzechStatisticalOfficeProvider.__instance__

    def preload_cache(self):
        sw = Stopwatch()
        with sw(f'Loading CSU data from cache'):
            with get_czech_statistical_office_connection() as conn:
                db = conn['scraped']
                collection = db['czech_statistical_office_2018']
                results = collection.find({})
                tmp = {self.normalize_county_name(county): {} for county in self.counties}
                for result in results:
                    data = result['data']
                    for county, features in data.items():
                        for column, value in features.items():
                            col_name = f'{column}'
                            tmp[county][col_name] = value
                return tmp

    def fix_county_corner_cases(self, county):
        if county == 'hlavní město praha':
            return 'praha-západ'
        return county

    def normalize_county_name(self, name):
        county = str(name).lower().replace('okres', '').strip()
        return self.fix_county_corner_cases(county)

    def load_from_cache(self, county):
        if self.cache is None:
            self.cache = self.preload_cache()

        county = self.normalize_county_name(county)
        assert county in self.cache, f'Some inconsistency found in data: county [{county}] not found in the Czech statistical office data'
        return self.cache[county]

