import re
from app.providers.provider import Provider
from app.common.utils import text_lower_rm_accents, parse_int, parse_float, load_config
from app.common.exceptions import InternalException


class DescriptionProvider(Provider):
    # __instance__ = None
    
    def __init__(self):
        super().__init__()
        self.config = load_config('description.json', config_dir='app/providers/configs')
        self.regexes = self.config['regexes']


    # def get_instance():
    #     if DescriptionProvider.__instance__ == None:
    #         DescriptionProvider.__instance__ = DescriptionProvider()
    #     return DescriptionProvider.__instance__

    def apply_regex(self, description, regex_holder):
        data_type = regex_holder['data_type']
        column_id = regex_holder['id']
        regex = regex_holder['regex']
        if data_type == 'boolean':
            ret = re.search(regex, description, re.I | re.M)
            return column_id, 'true' if ret else 'false'
        elif data_type == 'integer':
            ret = re.search(regex, description, re.I | re.M)
            group_name = regex_holder['group_name']
            if ret:
                val = ret.group(group_name)
                return column_id, parse_int(val)
        elif data_type == 'float':
            ret = re.search(regex, description, re.I | re.M)
            group_name = regex_holder['group_name']
            if ret:
                val = ret.group(group_name)
                return column_id, parse_float(val)
        elif data_type == 'string':
            ret = re.search(regex, description, re.I | re.M)
            group_name = regex_holder['group_name']
            if ret:
                return column_id, ret.group(group_name)
        else:
            raise InternalException(f'Unimplemented data_type: [{data_type}]')
        return None, None

    def load_from_cache(self, description):
        results = {}
        for regex in self.regexes:
            normalized_description = text_lower_rm_accents(description)
            column_id, value = self.apply_regex(normalized_description, regex)

            if column_id and value is not None:
                results[column_id] = value
        return results
