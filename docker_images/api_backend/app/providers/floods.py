from app.common.utils import Stopwatch, get_postgis_floods_connection
from app.providers.provider import Provider
import pandas.io.sql as sqlio


class FloodsProvider(Provider):

    def __init__(self) -> None:
        super().__init__()
        self.conn = get_postgis_floods_connection()

    def get_query(self, lon, lat):
        query = f"""
                WITH
                    flood5years as (SELECT (CASE WHEN COUNT(*) != 0 THEN 'true' ELSE 'false' END) AS "flood_5_years_nom_boolean" FROM flood_5_years WHERE ST_CONTAINS(geom, ST_SETSRID(ST_MAKEPOINT({lon}, {lat}), 4326))),
                    flood20years as (SELECT (CASE WHEN COUNT(*) != 0 THEN 'true' ELSE 'false' END) AS "flood_20_years_nom_boolean" FROM flood_20_years WHERE ST_CONTAINS(geom, ST_SETSRID(ST_MAKEPOINT({lon}, {lat}), 4326))),
                    flood100years as (SELECT (CASE WHEN COUNT(*) != 0 THEN 'true' ELSE 'false' END) AS "flood_100_years_nom_boolean" FROM flood_100_years WHERE ST_CONTAINS(geom, ST_SETSRID(ST_MAKEPOINT({lon}, {lat}), 4326))),
                    floodactiveareas as (SELECT (CASE WHEN COUNT(*) != 0 THEN 'true' ELSE 'false' END) AS "flood_active_area_nom_boolean" FROM flood_active_areas WHERE ST_CONTAINS(geom, ST_SETSRID(ST_MAKEPOINT({lon}, {lat}), 4326)))
                SELECT flood5years.*, flood20years.*, flood100years.*, floodactiveareas.* FROM flood5years CROSS JOIN flood20years CROSS JOIN flood100years CROSS JOIN floodactiveareas;
                """
        return query

    def load_from_sql(self, lon, lat):
        query = self.get_query(lon, lat)
        df = sqlio.read_sql_query(query, self.conn)
        assert len(df) == 1, 'Exactly one row should be returned'
        return df.iloc[0].to_dict()

    # def get_cache_collection(self):
    #     with get_connection() as client:
    #         database = client['cache']
    #         return database[self.collection_cache_name]
    #
    # def cache_result(self, lon, lat, data, collection):
    #     item = {
    #         "lat": lat,
    #         "lon": lon,
    #         "config_id": self.config_id,
    #         "data": data
    #     }
    #     is_duplicate = {"lat": lat, "lon": lon}
    #     written = collection.update_one(is_duplicate, {"$setOnInsert": item}, upsert=True)
    #     if written.upserted_id:
    #         print(f'Cached osm_data with an id: [{written.upserted_id}]')

    def load_from_cache(self, lon, lat):
        # collection = self.get_cache_collection()
        # cached = collection.find_one({"lat": lat, "lon": lon})
        # if cached:
        #     return cached['data']

        sw = Stopwatch()
        with sw('Loading data from the PostgreSQL floods origin'):
            data = self.load_from_sql(lon, lat)
            # if data:
            #     self.cache_result(lon, lat, data, collection)
            return data
