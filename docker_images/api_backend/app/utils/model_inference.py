from os import path

class ModelInference(object):
    def __init__(self):
        self.config = load_config('model_inference', config_dir='app/models')

    def load_model(self)
        model_name = self.config['model_name']
        self.keras_model_path = path.join('api/models', model_name, '')
        self.model_stats_path = path.join('api/models', model_name, '')
