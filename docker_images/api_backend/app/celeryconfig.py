################################
####    General settings    ####
################################
beat_scheduler = 5
accept_content = ['pickle', 'json','application/text']

################################
####    Broker settings     ####
################################
broker_heartbeat = 5
broker_transport = 'redis'
broker_url = 'redis://redis:6379/0'

################################
####     Redis settings     ####
################################
redis_host = "redis"
redis_port = 6379
redis_db = 0

################################
####    Worker settings     ####
################################
imports = ('app.main_api_backend', )
worker_concurrency = 1
worker_disable_rate_limits = False
worker_redirect_stdouts = False
worker_redirect_stdouts_level = 'DEBUG'
# worker_redirect_stdouts_level = 'INFO'
# worker_redirect_stdouts_level = 'WARNING'
# worker_redirect_stdouts_level = 'ERROR'
# worker_redirect_stdouts_level = 'CRITICAL'

################################
####  Task related config   ####
################################
task_always_eager = False
task_acks_late = True
task_publish_retry = True
task_time_limit = 60
task_soft_time_limit = 40
task_ignore_result = False
task_serializer = 'pickle'

################################
##### Result related config ####
################################
result_backend = "redis"
result_expires = 600
result_serializer = 'pickle'
