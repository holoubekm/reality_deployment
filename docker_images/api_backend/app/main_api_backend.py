import sys
import time, requests
import traceback
import re
from os import path
from functools import wraps

MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), "..", "app"))
sys.path.append(MOD_COMMON_PATH)

from models.price_model_inference import PriceModelInference
from providers import AggregatedProvider
from app.common.exceptions import *
from app.celery import app
from celery.exceptions import SoftTimeLimitExceeded

def handle_exceptions(fn):
    @wraps(fn)
    def exception_handler(*args, **kwargs):
        # ret = fn(*args, **kwargs)
        # return {'result': ret}

        try:
            ret = fn(*args, **kwargs)
            return {'result': ret}
        except SoftTimeLimitExceeded as exc:
            # TODO log here, save to the database and send email
            return {'error': 'SoftTimeLimitExceeded', 'messsage': str(exc), 'traceback': traceback.format_exc()}
        except (DataMissingException, \
                UnexpectedFeatureException, \
                MissingFeatureException, \
                UnexpectedNominalValueException, \
                LocationOutOfTheCountryException, \
                MissingAPIFeaturesException, \
                UnexpectedAPIFeaturesException) as exc:
            # TODO log here, save to the database and send email
            return {'error': exc.__class__.__name__, 'messsage': str(exc), 'traceback': traceback.format_exc()}
        except Exception as exc:
            # TODO log here, save to the database and send email
            return {'error': exc.__class__.__name__, 'messsage': str(exc), 'traceback': traceback.format_exc()}
    return exception_handler


# Internal API for data scraping and model learning
@app.task(bind=True)
@handle_exceptions
def _internal_fetch_additional_data(self, lon, lat, description):
    '''
        Only for internal purposes!
        Fetches raw unnormalized data directly from different sources.
    '''
    provider = AggregatedProvider()
    return provider.fetch_additional_data(lon, lat, description)















@app.task(bind=True)
@handle_exceptions
def get_flat_price_estimator_all_features(self, with_details):
    '''
        Returns either a list of all features or dictionary with details
    '''
    estimator = PriceModelInference.get_flat_instance()
    return estimator.get_all_features(with_details=with_details)

@app.task(bind=True)
@handle_exceptions
def get_flat_price_construction_estimation(self):
    '''
        Returns a flat construction cost estimation using hardcoded shitty model
    '''
    return 1000*1000*5;
#    estimator = FlatConstructionCostModelInference()
#    return estimator.get_estimation(lon=lon, lat=lat, description=description, external_api_data=external_api_data, with_error_estimation=with_error_estimation)
#    return estimator.get_estimation()

@app.task(bind=True)
@handle_exceptions
def get_flat_price_estimator_api_features(self):
    '''
        Returns either a list of external (API) features or dictionary with details
    '''

    estimator = PriceModelInference.get_flat_instance()
    return estimator.get_api_features()

@app.task(bind=True)
@handle_exceptions
def get_flat_price_estimator_all_features_importance(self):
    '''
        Returns a list of all features with appropriate weight learnt by the model
    '''
    estimator = PriceModelInference.get_flat_instance()
    return estimator.get_all_features_importance()

@app.task(bind=True)
@handle_exceptions
def get_flat_price_estimator_api_features_importance(self):
    '''
        Returns a list of external (API) features with appropriate weight learnt by the model
    '''
    estimator = PriceModelInference.get_flat_instance()
    return estimator.get_api_features_importance()

@app.task(bind=True)
@handle_exceptions
def get_flat_price_estimator_testing_error_statistics(self):
    '''
        Returns a flat price estimator error statistics. 
        List of price-based buckets each with error mean, deviation and number of flats in the bucket.
    '''
    estimator = PriceModelInference.get_flat_instance()
    return estimator.get_testing_error_statistics()

@app.task(bind=True)
@handle_exceptions
def get_flat_price_estimator_estimation(self, lon, lat, external_api_data, with_error_estimation=False):
    '''
        Returns a flat price estimation using the flat-related model
        Automatically fetches all necessary data in the background
    '''
    estimator = PriceModelInference.get_flat_instance()
    return estimator.get_estimation(lon=lon, lat=lat, external_api_data=external_api_data, with_error_estimation=with_error_estimation)






















@app.task(bind=True)
@handle_exceptions
def get_house_price_estimator_all_features(self, with_details):
    '''
        Returns either a list of all features or dictionary with details
    '''
    estimator = PriceModelInference.get_house_instance()
    return estimator.get_all_features(with_details=with_details)

@app.task(bind=True)
@handle_exceptions
def get_house_price_construction_estimation(self):
    '''
        Returns a house construction cost estimation using hardcoded shitty model
    '''
    return 1000*1000*5;
#    estimator = FlatConstructionCostModelInference()
#    return estimator.get_estimation(lon=lon, lat=lat, description=description, external_api_data=external_api_data, with_error_estimation=with_error_estimation)
#    return estimator.get_estimation()

@app.task(bind=True)
@handle_exceptions
def get_house_price_estimator_api_features(self):
    '''
        Returns either a list of external (API) features or dictionary with details
    '''

    estimator = PriceModelInference.get_house_instance()
    return estimator.get_api_features()

@app.task(bind=True)
@handle_exceptions
def get_house_price_estimator_all_features_importance(self):
    '''
        Returns a list of all features with appropriate weight learnt by the model
    '''
    estimator = PriceModelInference.get_house_instance()
    return estimator.get_all_features_importance()

@app.task(bind=True)
@handle_exceptions
def get_house_price_estimator_api_features_importance(self):
    '''
        Returns a list of external (API) features with appropriate weight learnt by the model
    '''
    estimator = PriceModelInference.get_house_instance()
    return estimator.get_api_features_importance()

@app.task(bind=True)
@handle_exceptions
def get_house_price_estimator_testing_error_statistics(self):
    '''
        Returns a house price estimator error statistics. 
        List of price-based buckets each with error mean, deviation and number of houses in the bucket.
    '''
    estimator = PriceModelInference.get_house_instance()
    return estimator.get_testing_error_statistics()

@app.task(bind=True)
@handle_exceptions
def get_house_price_estimator_estimation(self, lon, lat, external_api_data, with_error_estimation=False):
    '''
        Returns a house price estimation using the house-related model
        Automatically fetches all necessary data in the background
    '''
    estimator = PriceModelInference.get_house_instance()
    return estimator.get_estimation(lon=lon, lat=lat, external_api_data=external_api_data, with_error_estimation=with_error_estimation)



















# Public-facing surroundings related API
@app.task(bind=True)
@handle_exceptions
def get_surroundings_types(self):
    '''
        Returns a list of all surrounding types
    '''
    provider = AggregatedProvider()
    return provider.get_surroundings_types()

@app.task(bind=True)
@handle_exceptions
def get_surroundings(self, lon, lat):
    '''
        Returns a list of all surrounding near the specifie point.
        All possible types can be retrieved using `get_surroundings_types`
    '''
    provider = AggregatedProvider()
    provider.test_location_in_the_country(lon=lon, lat=lat)
    return provider.get_surroundings(lon=lon, lat=lat)


# Public-facing floods related API
@app.task(bind=True)
@handle_exceptions
def get_area_flood_risk(self, lon, lat):
    '''
        Returns a dictionary of the following boolean flags:
        has_5_years_flood_risk - There is a risk of the 5 year flood
        has_20_years_flood_risk - There is a risk of the 20 year flood
        has_100_years_flood_risk - There is a risk of the 100 year flood
        is_active_flood_area - Flood active area, it's prohibited to build there. No insurance for you as well.
    '''
    provider = AggregatedProvider()
    provider.test_location_in_the_country(lon=lon, lat=lat)
    return provider.get_area_flood_risk(lon=lon, lat=lat)







@app.task(bind=True)
@handle_exceptions
def get_url_comparison(self, sale_url):

    regex = r'https?://(www\.)?sreality\.cz/detail/.*?(?P<hash_id>\d{5,})'
    result = re.match(regex, sale_url)

    if result is None:
        raise MissingFeatureException(f'Not matching url for sreality - expected patern is [{regex}]')
    hash_id = result.groupdict()['hash_id']

    from providers import SrealityDetailsScraper, SrealityFeaturesMiner
    item = SrealityDetailsScraper().scrape(hash_id)
    features = SrealityFeaturesMiner().mine(item)

    if 'sreal_typ_nemovitosti_nom_string' not in features['data']:
        raise MissingFeatureException('The specified entry has does not have a type assigned. Impossible to determine the estate type.')

    sreality_type = features['data']['sreal_typ_nemovitosti_nom_string']
    supported_flat_types = {'1_kk', '1_1', '2_kk', '2_1', '3_kk', '3_1', '4_kk', '4_1', '5_kk', '5_1', '6_a_vice', 'atypicky'}
    supported_estate_types = {'chaty', 'rodinne_domy', 'vily', 'chalupy'}

    if sreality_type not in supported_flat_types and sreality_type not in supported_estate_types:
        raise MissingFeatureException(f'Not supported estate or flat type: [{sreality_type}]. Supported estate types: [{supported_estate_types}], flat types: [{supported_flat_types}]')

    if sreality_type in supported_flat_types:
        estimator = PriceModelInference.get_flat_instance()
    else:
        estimator = PriceModelInference.get_house_instance()

    if 'coords' not in features:
        raise MissingFeatureException(f'GPS data missing from the advertisement')

    coords = features['coords']
    lon, lat = coords['lon'], coords['lat']
    estimation = estimator.get_estimation(lon=lon, lat=lat, external_api_data=features['data'], description=features['description'], with_error_estimation=True, sanitize_api_data=False)

    return {'price_estimation': estimation, 'original_price': features['price']}

