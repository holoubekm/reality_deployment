from celery                 import Celery, signature
# from tools.request          import CeleryTaskError

import sys

celery_app = Celery()
celery_app.config_from_object('celeryconfig')

def run_celery_task_by_name(name, kwargs):
    # params = {
        # "request" : request,
        # "params"  : kwargs
    # }
    # print('task RUN', file=sys.stderr) 

    task = signature(f'app.main_api_backend.{name}', kwargs=kwargs)
    result = task.apply_async().get()

    # print('task END', file=sys.stderr) 
    # if 'error' in result:
        # raise CeleryTaskError(result["message"], {'error': result["error"], 'traceback':result["traceback"]})

    return result