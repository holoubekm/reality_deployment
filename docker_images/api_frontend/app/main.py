#!/usr/bin/env python3

from flask import Flask, request 
import datetime
import logging

from routes import init_routes
# from tools.toplevelrequest import ToplevelRequest

flask_app = Flask(__name__)
init_routes(flask_app)


# TODO: MAYBE REMOVE ????
# @flask_app.errorhandler(404)
# def not_found(error):
#     resp = make_response(render_template('./error.html'), 404)
#     return resp

# @flask_app.route('/ping')
# @ToplevelRequest
# def ping(*extra, **kwextra):
#     ret = {
#         "time": datetime.datetime.now()
#     }
#     return jsonify(**ret)


if __name__ == '__main__':
    flask_app.run(debug=True, host='0.0.0.0')
