#!/usr/bin/env python3

import datetime
import logging

import sys
from os import path
MOD_COMMON_PATH = path.abspath(path.join(path.dirname(__file__), '..', '..', 'api_backend', 'app'))
sys.path.append(MOD_COMMON_PATH)

from celery_app import run_celery_task_by_name

def test():
    kwargs = {
        'lat': 50.08804, 
        'lon': 14.42076,
        'description': '',
        'external_api_data': {
            "description_built_area_ord_float": 100,
            "description_flat_layout_nom_string": "3+1",
            "description_has_a_common_space_nom_boolean": True,
            "description_has_a_fireplace_nom_boolean": True,
            "description_has_a_private_water_source_nom_boolean": True,
            "description_has_a_project_nom_boolean": True,
            "description_has_a_technical_documentation_nom_boolean": True,
            "description_has_a_tiled_stove_nom_boolean": True,
            "description_has_air_conditioning_nom_boolean": True,
            "description_has_attic_nom_boolean": True,
            "description_has_baby_stroller_room_nom_boolean": True,
            "description_has_balcony_nom_boolean": True,
            "description_has_bathroom_nom_boolean": True,
            "description_has_bathtub_nom_boolean": True,
            "description_has_bidet_nom_boolean": True,
            "description_has_bike_room_nom_boolean": True,
            "description_has_brick_core_nom_boolean": True,
            "description_has_builtin_wardrobe_nom_boolean": True,
            "description_has_cellar_nom_boolean": True,
            "description_has_central_heating_nom_boolean": True,
            "description_has_civic_amenities_nom_boolean": True,
            "description_has_dining_room_nom_boolean": True,
            "description_has_dishwasher_nom_boolean": True,
            "description_has_doorway_nom_boolean": True,
            "description_has_dressing_room_nom_boolean": True,
            "description_has_drying_machine_nom_boolean": True,
            "description_has_electric_boiler_nom_boolean": True,
            "description_has_electric_heating_nom_boolean": True,
            "description_has_electric_stove_nom_boolean": True,
            "description_has_floating_floor_nom_boolean": True,
            "description_has_floor_carpet_nom_boolean": True,
            "description_has_floor_heating_nom_boolean": True,
            "description_has_floor_parquets_nom_boolean": True,
            "description_has_floor_tiling_nom_boolean": True,
            "description_has_freezer_nom_boolean": True,
            "description_has_french_windows_nom_boolean": True,
            "description_has_fridge_nom_boolean": True,
            "description_has_garage_nom_boolean": True,
            "description_has_garder_nom_boolean": True,
            "description_has_gas_boiler_nom_boolean": True,
            "description_has_gas_heating_nom_boolean": True,
            "description_has_gas_stove_nom_boolean": True,
            "description_has_high_pressure_laminate_core_nom_boolean": True,
            "description_has_induction_cooking_plate_nom_boolean": True,
            "description_has_kids_room_nom_boolean": True,
            "description_has_kitchen_nom_boolean": True,
            "description_has_kitchen_unit_nom_boolean": True,
            "description_has_kitchenette_nom_boolean": True,
            "description_has_large_room_nom_boolean": True,
            "description_has_living_room_nom_boolean": True,
            "description_has_lobby_nom_boolean": True,
            "description_has_loggia_nom_boolean": True,
            "description_has_low_expenditures_nom_boolean": True,
            "description_has_low_fees_nom_boolean": True,
            "description_has_mortgage_funding_nom_boolean": True,
            "description_has_new_bathroom_nom_boolean": True,
            "description_has_new_doors_nom_boolean": True,
            "description_has_new_facade_nom_boolean": True,
            "description_has_nice_view_nom_boolean": True,
            "description_has_no_transfer_tax_nom_boolean": True,
            "description_has_oven_nom_boolean": True,
            "description_has_pantry_nom_boolean": True,
            "description_has_parking_nom_boolean": True,
            "description_has_parking_spot_nom_boolean": True,
            "description_has_plastic_windows_nom_boolean": True,
            "description_has_pool_nom_boolean": True,
            "description_has_private_ownership_nom_boolean": True,
            "description_has_sauna_nom_boolean": True,
            "description_has_separate_heating_nom_boolean": True,
            "description_has_separate_toilet_nom_boolean": True,
            "description_has_separate_workshop_nom_boolean": True,
            "description_has_shared_ownership_nom_boolean": True,
            "description_has_shower_nom_boolean": True,
            "description_has_sleeping_room_nom_boolean": True,
            "description_has_spacious_dressing_room_nom_boolean": True,
            "description_has_sun_blinders_nom_boolean": True,
            "description_has_terrace_nom_boolean": True,
            "description_has_the_final_inspection_carried_out_nom_boolean": True,
            "description_has_thermal_insulation_nom_boolean": True,
            "description_has_toilet_nom_boolean": True,
            "description_has_total_floor_count_ord_integer": 4,
            "description_has_tv_nom_boolean": True,
            "description_has_underground_parking_nom_boolean": True,
            "description_has_unpaid_annuity_nom_boolean": True,
            "description_has_utility_room_nom_boolean": True,
            "description_has_vinyl_floor_nom_boolean": True,
            "description_has_washing_machine_nom_boolean": True,
            "description_has_washing_room_nom_boolean": True,
            "description_has_wheel_chair_access_nom_boolean": True,
            "description_has_wooden_ceiling_nom_boolean": True,
            "description_has_wooden_windows_nom_boolean": True,
            "description_is_a_cottage_nom_boolean": True,
            "description_is_a_family_house_nom_boolean": True,
            "description_is_a_hut_nom_boolean": True,
            "description_is_after_complete_reconstruction_nom_boolean": True,
            "description_is_after_partial_reconstruction_nom_boolean": True,
            "description_is_after_reconstruction_nom_boolean": True,
            "description_is_attic_flat_nom_boolean": True,
            "description_is_before_reconstruction_nom_boolean": True,
            "description_is_brick_house_nom_boolean": True,
            "description_is_city_center_nom_boolean": True,
            "description_is_desired_location_nom_boolean": True,
            "description_is_easily_accesible_nom_boolean": True,
            "description_is_east_oriented_nom_boolean": True,
            "description_is_exclusive_nom_boolean": True,
            "description_is_extension_flat_nom_boolean": True,
            "description_is_fully_furnished_nom_boolean": True,
            "description_is_ideal_for_investment_nom_boolean": True,
            "description_is_in_bad_condition_nom_boolean": True,
            "description_is_in_good_condition_nom_boolean": True,
            "description_is_in_original_condition_nom_boolean": True,
            "description_is_in_the_center_nom_boolean": True,
            "description_is_interesting_nom_boolean": True,
            "description_is_low_energy_demanding_nom_boolean": True,
            "description_is_mezonet_flat_nom_boolean": True,
            "description_is_multigeneration_nom_boolean": True,
            "description_is_near_the_center_nom_boolean": True,
            "description_is_newly_built_nom_boolean": True,
            "description_is_newly_furnished_nom_boolean": True,
            "description_is_north_oriented_nom_boolean": True,
            "description_is_on_behalf_of_reality_estate_nom_boolean": True,
            "description_is_on_floor_ord_integer": 2,
            "description_is_partially_furnished_nom_boolean": True,
            "description_is_prefab_house_nom_boolean": True,
            "description_is_pretty_place_nom_boolean": True,
            "description_is_quiet_place_nom_boolean": True,
            "description_is_ready_to_move_in_nom_boolean": True,
            "description_is_residential_nom_boolean": True,
            "description_is_shared_transfer_nom_boolean": True,
            "description_is_south_oriented_nom_boolean": True,
            "description_is_spacious_nom_boolean": True,
            "description_is_suitable_for_a_family_nom_boolean": True,
            "description_is_two_generation_nom_boolean": True,
            "description_is_west_oriented_nom_boolean": True,
            "description_is_without_commitment_nom_boolean": True,
            "description_is_without_elevator_nom_boolean": True,
            "description_is_without_fees_nom_boolean": True,
            "description_is_without_provision_nom_boolean": True,
            "description_is_without_tax_nom_boolean": True,
            "description_land_area_ord_float": 10,
            "sreal_balkon_nom_boolean": True,
            "sreal_balkon_ord_float": 20,
            "sreal_bazen_nom_boolean": True,
            "sreal_bezbarierovy_nom_boolean": True,
            "sreal_doprava__nom_boolean": True,
            "sreal_doprava_autobus_nom_boolean": True,
            "sreal_doprava_dalnice_nom_boolean": True,
            "sreal_doprava_mhd_nom_boolean": True,
            "sreal_doprava_silnice_nom_boolean": True,
            "sreal_doprava_vlak_nom_boolean": True,
            "sreal_elektrina_120v_nom_boolean": True,
            "sreal_elektrina_230v_nom_boolean": True,
            "sreal_elektrina_400v_nom_boolean": True,
            "sreal_energeticka_narocnost_budovy_nom_string": "",
            "sreal_garaz_nom_boolean": True,
            "sreal_garaz_ord_integer": 3,
            "sreal_komunikace_asfaltova_nom_boolean": True,
            "sreal_komunikace_betonova_nom_boolean": True,
            "sreal_komunikace_dlazdena_nom_boolean": True,
            "sreal_komunikace_neupravena_nom_boolean": True,
            "sreal_lodzie_nom_boolean": True,
            "sreal_lodzie_ord_float": 5,
            "sreal_odpad_cov_pro_cely_objekt_nom_boolean": True,
            "sreal_odpad_jimka_nom_boolean": True,
            "sreal_odpad_septik_nom_boolean": True,
            "sreal_odpad_verejna_kanalizace_nom_boolean": True,
            "sreal_parkovani_nom_boolean": True,
            "sreal_parkovani_ord_integer": 2,
            "sreal_plocha_bazenu_ord_float": 10,
            "sreal_plocha_podlahova_ord_float": 100,
            "sreal_plocha_zahrady_ord_float": 1000,
            "sreal_plocha_zastavena_ord_float": 150,
            "sreal_plyn_individualni_nom_boolean": True,
            "sreal_plyn_plynovod_nom_boolean": True,
            "sreal_pocet_bytu_ord_integer": 5,
            "sreal_podlazi_nom_string": "",
            "sreal_prevod_do_ov_nom_string": "ano",
            "sreal_pudni_vestavba_nom_boolean": True,
            "sreal_rok_kolaudace_ord_float": 1960,
            "sreal_rok_rekonstrukce_ord_float": 1950,
            "sreal_sklep_nom_boolean": True,
            "sreal_stav_nom_string": "",
            "sreal_stav_objektu_nom_string": "dobry",
            "sreal_stavba_nom_string": "cihlova",
            "sreal_telekomunikace_internet_nom_boolean": True,
            "sreal_sklep_ord_float": 10,
            "sreal_telekomunikace_kabelova_televize_nom_boolean": True,
            "sreal_telekomunikace_kabelove_rozvody_nom_boolean": True,
            "sreal_telekomunikace_ostatni_nom_boolean": True,
            "sreal_telekomunikace_satelit_nom_boolean": True,
            "sreal_telekomunikace_telefon_nom_boolean": True,
            "sreal_terasa_nom_boolean": True,
            "sreal_terasa_ord_float": 10,
            "sreal_topeni_jine_nom_boolean": True,
            "sreal_topeni_lokalni_elektricke_nom_boolean": True,
            "sreal_topeni_lokalni_plynove_nom_boolean": True,
            "sreal_topeni_lokalni_tuha_paliva_nom_boolean": True,
            "sreal_topeni_ustredni_dalkove_nom_boolean": True,
            "sreal_topeni_ustredni_elektricke_nom_boolean": True,
            "sreal_topeni_ustredni_plynove_nom_boolean": True,
            "sreal_topeni_ustredni_tuha_paliva_nom_boolean": True,
            "sreal_typ_bytu_nom_string": "",
            "sreal_typ_nemovitosti_nom_string": "3_1",
            "sreal_umisteni_objektu_nom_string": "centrum_obce",
            "sreal_uzitna_plocha_ord_float": 100,
            "sreal_vlastnictvi_nom_string": "osobni",
            "sreal_voda_dalkovy_vodovod_nom_boolean": True,
            "sreal_voda_mistni_zdroj_nom_boolean": True,
            "sreal_vybaveni_nom_boolean": True,
            "sreal_vybaveni_nom_string": "",
            "sreal_vyska_stropu_ord_float": 20,
            "sreal_vytah_nom_boolean": True
            },
        'with_error_estimation': True
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_estimation', kwargs=kwargs)
    print(ret)

    kwargs = {
        'lat': 50.08804, 
        'lon': 14.42076,
        'description': 'asdfasdf'
    }
    ret = run_celery_task_by_name('_internal_fetch_additional_data', kwargs=kwargs)
    print(ret)


    kwargs = {
        'with_details': False
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_all_features', kwargs=kwargs)
    print(ret)

    ret = run_celery_task_by_name('get_flat_price_construction_estimation', kwargs={})
    print(ret)

    kwargs = {
        'with_details': False
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_api_features', kwargs=kwargs)
    print(ret)

    ret = run_celery_task_by_name('get_flat_price_estimator_all_features_importance', kwargs={})
    print(ret)

    ret = run_celery_task_by_name('get_flat_price_estimator_api_features_importance', kwargs={})
    print(ret)

    ret = run_celery_task_by_name('get_flat_price_estimator_testing_error_statistics', kwargs={})
    print(ret)

    ret = run_celery_task_by_name('get_flat_surroundings_types', kwargs={})
    print(ret)
        
    kwargs = {
        'lat': 50.08804, 
        'lon': 14.42076
    }
    ret = run_celery_task_by_name('get_flat_surroundings', kwargs=kwargs)
    print(ret)        
    
    kwargs = {
        'lat': 50.08804, 
        'lon': 14.42076
    }
    ret = run_celery_task_by_name('get_area_flood_risk', kwargs=kwargs)
    print(ret)
    

if __name__ == '__main__':
    test()
