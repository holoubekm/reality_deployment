# import routes.celeryconfig
# import routes.celery
# import routes.v001

from routes.v001.property import app

def init_routes(flask_app):
    print("INIT API /v001 ROUTES");
    flask_app.register_blueprint(app, url_prefix='/v001/property')

