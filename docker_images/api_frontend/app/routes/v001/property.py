import sys
from flask import Blueprint, request, jsonify
from functools import wraps

from celery_app import run_celery_task_by_name
app = Blueprint('v001', __name__)


api_keys = {'6e60ee07-21cc-4c8c-b1ec-80496f77783f', '690bc6cc-f1e3-49e7-addf-d8b64875c77c', '639ab52f-d145-4c27-95f1-68a5da3baa8a', '2c4180ad-1382-4f4b-b203-6183c77c1e2f', 'ababa6a1-887d-416d-a6af-b6cdcc8bb452', '20ce3b39-f878-43ed-9e1a-87fde631c8de', 'aad83828-9515-4d1c-b5a8-bb0685c2fd54', 'fd3fe096-c91c-427c-a8af-f85e2a62c871', '2b6162aa-7d9a-430f-9fcc-d82fb64fce5f', '41418424-d201-4cc3-a2e0-3a3f81dd89c5'}

class InvalidOrMissingApiKeyException(Exception):
    def __init__(self, *args: object, **kwargs: object) -> None:
        super().__init__(*args, **kwargs)


def check_api_token(fn):
    @wraps(fn)
    def access_control_worker(*args, **kwargs):
        api_key = request.headers.get('x-api-key')
        if api_key not in api_keys:
            raise InvalidOrMissingApiKeyException(f'Please provide a valid API token via x-api-key header. Current value: [{api_key}]')
        return fn(*args, **kwargs)
    return access_control_worker







route = "/flat/price_estimator"
@app.route(f'{route}/all_features', methods=["POST"])
@check_api_token
def get_flat_price_estimator_all_features():
    with_details = request.args.get('with_details', default='false', type=str) in ['true']
    kwargs = {
        'with_details': with_details
    }
    ret = run_celery_task_by_name('get_flat_price_estimator_all_features', kwargs=kwargs)
    return jsonify(ret)
    

@app.route(f'{route}/api_features', methods=["POST"])
@check_api_token
def get_flat_price_estimator_api_features():
    ret = run_celery_task_by_name('get_flat_price_estimator_api_features', kwargs={})
    return jsonify(ret)


@app.route(f'{route}/all_features/importance', methods=["POST"])
@check_api_token
def get_flat_price_estimator_all_features_importance():
    ret = run_celery_task_by_name('get_flat_price_estimator_all_features_importance', kwargs={})
    return jsonify(ret)


@app.route(f'{route}/api_features/importance', methods=["POST"])
@check_api_token
def get_flat_price_estimator_api_features_importance():
    ret = run_celery_task_by_name('get_flat_price_estimator_api_features_importance', kwargs={})
    return jsonify(ret)


@app.route(f'{route}/testing_error_statistics', methods=["POST"])
@check_api_token
def get_flat_price_estimator_testing_error_statistics():
    ret = run_celery_task_by_name('get_flat_price_estimator_testing_error_statistics', kwargs={})
    return jsonify(ret)


@app.route(f'{route}/market_value', methods=["POST"])
@check_api_token
def get_flat_price_estimator_estimation():
    data = request.get_json()
    keys = ['lon', 'lat', 'external_api_data', 'with_error_estimation']

    kwargs = {}
    for key in keys:
        if key not in data:
            raise Exception(f'Missing json field in data: [{key}]')
        kwargs[key] = data[key]

    ret = run_celery_task_by_name('get_flat_price_estimator_estimation', kwargs=kwargs)
    return jsonify(ret)







route = "/house/price_estimator"
@app.route(f'{route}/all_features', methods=["POST"])
@check_api_token
def get_house_price_estimator_all_features():
    with_details = request.args.get('with_details', default='false', type=str) in ['true']
    kwargs = {
        'with_details': with_details
    }
    ret = run_celery_task_by_name('get_house_price_estimator_all_features', kwargs=kwargs)
    return jsonify(ret)
    

@app.route(f'{route}/api_features', methods=["POST"])
@check_api_token
def get_house_price_estimator_api_features():
    ret = run_celery_task_by_name('get_house_price_estimator_api_features', kwargs={})
    return jsonify(ret)


@app.route(f'{route}/all_features/importance', methods=["POST"])
@check_api_token
def get_house_price_estimator_all_features_importance():
    ret = run_celery_task_by_name('get_house_price_estimator_all_features_importance', kwargs={})
    return jsonify(ret)


@app.route(f'{route}/api_features/importance', methods=["POST"])
@check_api_token
def get_house_price_estimator_api_features_importance():
    ret = run_celery_task_by_name('get_house_price_estimator_api_features_importance', kwargs={})
    return jsonify(ret)


@app.route(f'{route}/testing_error_statistics', methods=["POST"])
@check_api_token
def get_house_price_estimator_testing_error_statistics():
    ret = run_celery_task_by_name('get_house_price_estimator_testing_error_statistics', kwargs={})
    return jsonify(ret)


@app.route(f'{route}/market_value', methods=["POST"])
@check_api_token
def get_house_price_estimator_estimation():
    data = request.get_json()
    keys = ['lon', 'lat', 'external_api_data', 'with_error_estimation']

    kwargs = {}
    for key in keys:
        if key not in data:
            raise Exception(f'Missing json field in data: [{key}]')
        kwargs[key] = data[key]

    ret = run_celery_task_by_name('get_house_price_estimator_estimation', kwargs=kwargs)
    return jsonify(ret)









route = ""
@app.route(f'{route}/surroundings', methods=["POST"])
@check_api_token
def get_surroundings():
    data = request.get_json()
    keys = ['lon', 'lat']

    kwargs = {}
    for key in keys:
        if key not in data:
            raise Exception(f'Missing json field in data: [{key}]')
        kwargs[key] = data[key]

    ret = run_celery_task_by_name('get_surroundings', kwargs=kwargs)
    return jsonify(ret)


@app.route(f'{route}/flood_risk', methods=["POST"])
@check_api_token
def get_area_flood_risk():
    data = request.get_json()
    keys = ['lon', 'lat']

    kwargs = {}
    for key in keys:
        if key not in data:
            raise Exception(f'Missing json field in data: [{key}]')
        kwargs[key] = data[key]

    ret = run_celery_task_by_name('get_area_flood_risk', kwargs=kwargs)
    return jsonify(ret)


@app.route(f'{route}/surrounding_types', methods=["POST"])
@check_api_token
def get_surroundings_types():
    ret = run_celery_task_by_name('get_surroundings_types', kwargs={})
    return jsonify(ret)









route = ""
@app.route(f'{route}/url/comparison', methods=["POST"])
@check_api_token
def get_url_comparison():
    data = request.get_json()
    keys = ['sale_url']

    kwargs = {}
    for key in keys:
        if key not in data:
            raise Exception(f'Missing json field in data: [{key}]')
        kwargs[key] = data[key]

    ret = run_celery_task_by_name('get_url_comparison', kwargs=kwargs)
    return jsonify(ret)
