# from flask import request, jsonify
# from functools import wraps
# import yaml

# from .request import RequestError

# def parse_yaml(filename):
#     # Read YAML file

#     data_loaded = None
#     with open(filename, 'r') as stream:
#         data_loaded = yaml.load(stream)

#     if data_loaded is None:
#         raise Exception(f'Oops, cannot load yaml config {filename}')

#     return data_loaded


# ##relative to the app directory ( weird )
# CustomersConfig = parse_yaml("./customers.yaml")

# #
# # Decorator for searching for the customers credentials
# #   search in the customers database
# #   and throws error if the customer not matches (both name and key)
# #

# def UserProviderControl(original_function):
#     @wraps(original_function)
#     def wrapped(*args, **kwargs):
#         if request.json is None:
#             raise RequestError("POST data are not valid JSON object", {})


#         customer = CustomersConfig[request.json.get('name')]

#         if (customer is None) or (customer["key"] != request.json.get('key')):
#             raise RequestError("Bad credentials", {})

#         kwargs = {
#             **kwargs,
#             "customer": customer
#         }

#         return original_function(*args, **kwargs)

#     return wrapped


# #
# # Decorator for providing access just to the customer with certain permissions
# #   and throws error if the customer not have enough permissions
# #

# def AvailableIf(name):
#     def restrictor(original_function):
#         @wraps(original_function)
#         def wrapped(*args, **kwargs):
#             customer = kwargs["customer"]

#             if name not in customer["permissions"]:
#                 raise RequestError("Insufficient permission", {})

#             return original_function(*args, **kwargs)

#         return wrapped
#     return restrictor

