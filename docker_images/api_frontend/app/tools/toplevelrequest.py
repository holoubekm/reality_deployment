# import uuid
# from functools import wraps
# from time import time
# from datetime import datetime
# from flask import request
# from flask import jsonify


# from .request import UserError


# #
# # Decorator for handling top request
# #   deals with connecting into mongo config database
# #   and handles error reporting to the user
# #



# class Logger(object):
#     def __init__(self, request, customer, args):
#         self.request = request
#         self.customer = customer
#         self.starttime = time()

#         self.logQuery("start", {"args": args});

#     def logQuery(self, type, msg, status=True):

#         data = {
#             "type": type,
#             "uuid": self.request,
#             "customer": self.customer,
#             "time": time() - self.starttime,
#             "msg": msg,
#             "status": status
#         }

#         #TODO: add logging into database
# #        print(data)

#     def msg(self, *args, **kwargs):
#         self.logQuery("msg", *args, **kwargs)

#     def end(self, *args, **kwargs):
#         self.logQuery("end", *args, **kwargs)

#     def __call__(self, *args, **kwargs):
#         self.msg(*args, **kwargs)




# def ToplevelRequest(original_function):
#     @wraps(original_function)
#     def wrapped(*args, **kwargs):

#         if "customer" in kwargs:
#             customer = kwargs["customer"]
#         else:
#             customer = None

#         request_uuid = uuid.uuid4()

#         logger = Logger(request_uuid, customer, request.args)

#         kwargs = {
#             **kwargs,
#             "logger": logger,
#             "request": {
#                 "uuid" : request_uuid,
#             }
#         }

#         retval = None
#         retlog = None
#         retstat= 0 # 1 = OK, 0 = undefined, -1 = user error, -2 = internal error


#         try:
#             retval = retlog = original_function(*args, **kwargs)
#             retstat = 1
#         except UserError as e:
#             retval = retlog = ({"error": str(e), "params": e.get_response()}),500
#             retstat = -1
#         except ValueError as e:
#             retval = ({"error": "Internal server error, please try again later."}),500
#             retlog = ({"error": str(e), "params": e.get_response()}),500
#             retstat = -2

#         logger.end(retlog, retstat);

#         if isinstance(retval, tuple):
#             return jsonify(retval[0]), retval[1]
#         else:
#             return jsonify(retval)

#     return wrapped