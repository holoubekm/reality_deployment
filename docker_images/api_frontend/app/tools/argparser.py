from webargs import flaskparser
from .request import InputValidationError

parser = flaskparser.FlaskParser()
@parser.error_handler
def handle_error(error, req, schema, status_code, headers):
    raise InputValidationError(f"Query structure error ({status_code})", error.messages)
