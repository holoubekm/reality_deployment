#
#  Custom exceptions which content is meant to be shown to the user
#


class ValueError(Exception):

    response = {}

    def __init__(self, message, response):

        # Call the base class constructor with the parameters it needs
        super(ValueError, self).__init__(message)

        self.response = response

    def get_response(self):
        return self.response


#
#   UserError content is shown to the user
#

class UserError(ValueError):
    pass


class RequestError(UserError):
    pass

class CeleryTaskError(ValueError):
    pass

class InputValidationError(UserError):
    pass
