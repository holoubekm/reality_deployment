#!/bin/sh

POSTGRE_HOST=127.0.0.1
POSTGRE_PORT=5432

PBF_DATA=http://download.geofabrik.de/europe/czech-republic-latest.osm.pbf
TMP_DIR=/tmp/data/
PBF_FILE=${TMP_DIR}data.osm.pbf
O5M_FILE=${TMP_DIR}data.o5m
OSM_FILTERED=${TMP_DIR}data.osm

until nc -z $POSTGRE_HOST $POSTGRE_PORT
do
    echo "Waiting for Postgre (127.0.0.1:5432) to start..."
    sleep 0.5
done

echo "Sleeping for 5s, just to be sure."
sleep 5



echo "********************************************"
echo "      Setting up the database and user      "
echo "********************************************"
export PGPASSWORD=mypass
psql --host="$POSTGRE_HOST" -c "CREATE USER myuser WITH SUPERUSER CREATEDB ENCRYPTED PASSWORD 'mypass'" || exit 1
createdb --host="$POSTGRE_HOST" -U myuser pgsnapshot || exit 1
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -c "GRANT ALL PRIVILEGES ON DATABASE pgsnapshot TO myuser" || exit 1

echo "Adding extensions postgis, hstore to DB"
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -c "CREATE EXTENSION postgis; CREATE EXTENSION hstore" || exit 1

echo "Generating schema"
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./schemas/pgsnapshot_schema_0.6.sql || exit 1
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./schemas/pgsnapshot_schema_0.6_action.sql || exit 1
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./schemas/pgsnapshot_schema_0.6_linestring.sql || exit 1
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./schemas/pgsnapshot_schema_0.6_bbox.sql || exit 1

echo "********************************************"
echo " Importing flood data of the Czech republic "
echo "********************************************"
shp2pgsql -s 2065:4326 -c -I -W "latin1" ./flood_data/D01_ZaplUzemi5Vody.shp flood_5_years | psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot || exit 1
shp2pgsql -s 2065:4326 -c -I -W "latin1" ./flood_data/D02_ZaplUzemi20Vody.shp flood_20_years | psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot || exit 1
shp2pgsql -s 2065:4326 -c -I -W "latin1" ./flood_data/D03_ZaplUzemi100Vody.shp flood_100_years | psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot || exit 1
shp2pgsql -s 2065:4326 -c -I -W "latin1" ./flood_data/D05_AktivniZony100Vody.shp flood_active_areas | psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot || exit 1
echo "Import finished..."
echo ""
echo ""

echo "********************************************"
echo "     Importing map of the Czech republic    "
echo "********************************************"

echo "Downloading the osm data from: \"$PBF_DATA\""
curl -L -f "${PBF_DATA}" --create-dirs -o "${PBF_FILE}" || exit 1

echo "Converting the osm data from: \"${PBF_FILE}\" to: \"${O5M_FILE}\""
osmconvert --out-o5m -o="${O5M_FILE}" "${PBF_FILE}" || exit 1

echo "Filtering the osm data from: \"${O5M_FILE}\" to: \"${OSM_FILTERED}\""
osmfilter "${O5M_FILE}" -t="${TMP_DIR}tempfile" --keep="aeroway amenity animal barrier club craft cycleway dog emergency healthcare highway horse leisure natural office power public_transport religion route shop social_facility sport tourism vending" --out-osm -o="${OSM_FILTERED}" || exit 1

echo "Importing the osm data from: \"$OSM_FILTERED\""
osmosis --read-xml "${OSM_FILTERED}" --log-progress --write-pgsql host=""$POSTGRE_HOST"" user="myuser" password="mypass" database="pgsnapshot" || exit 1

echo "Adding bbox columns"
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./sql/add_bbox.sql || exit 1

echo "Generating bboxes out of recursive relations"
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./sql/make_recursive_bboxes.sql || exit 1

echo "Creating the materialized view filter"
python3.6 ./sql/generate_category_filter.py || exit 1
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./sql/category_filter.sql || exit 1

echo "Generating categories"
python3.6 ./sql/generate_categories.py || exit 1
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./sql/add_categories.sql || exit 1

echo "Cleaning"
psql --host="$POSTGRE_HOST" -U myuser -d pgsnapshot -f ./sql/postprocess.sql || exit 1

rm -rf "${TMP_DIR}"
