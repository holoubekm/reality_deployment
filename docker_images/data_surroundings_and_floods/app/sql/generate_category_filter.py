from json import load as json_load

class Generator(object):
    def __init__(self):
        with open('./sql/generate_categories_config.json') as inp_file:
            self.config = json_load(inp_file)

    def generate_sql(self, file_name='./sql/category_filter.sql', table_name='all_filtered'):
        nodes_query_list = []
        ways_query_list = []
        rels_query_list = []
        for group in self.config['groups']:
            group_name = group['name']
            column_name = f'{group_name}'
            queries = group['values']

            for query in queries:
                
                nodes_condition_list = []
                ways_condition_list = []
                rels_condition_list = []

                for condition in query:
                    tag_name, tag_val = condition[0], condition[1]
                    if tag_val:
                        nodes_condition_list.append(f"nodes.tags->'{tag_name}' = '{tag_val}'")
                        ways_condition_list.append(f"ways.tags->'{tag_name}' = '{tag_val}'")
                        rels_condition_list.append(f"relations.tags->'{tag_name}' = '{tag_val}'")
                    else:
                        nodes_condition_list.append(f"exist(nodes.tags, '{tag_name}')")
                        ways_condition_list.append(f"exist(ways.tags, '{tag_name}')")
                        rels_condition_list.append(f"exist(relations.tags, '{tag_name}')")
                
                nodes_condition_str = ' AND '.join(nodes_condition_list)
                ways_condition_str = ' AND '.join(ways_condition_list)
                rels_condition_str = ' AND '.join(rels_condition_list)

                nodes_condition_str = f'({nodes_condition_str})'
                ways_condition_str = f'({ways_condition_str})'
                rels_condition_str = f'({rels_condition_str})'

                nodes_query_list.append(nodes_condition_str)
                ways_query_list.append(ways_condition_str)
                rels_query_list.append(rels_condition_str)

        nodes_query_str = ' OR '.join(nodes_query_list)
        ways_query_str = ' OR '.join(ways_query_list)
        rels_query_str = ' OR '.join(rels_query_list)

        subqueries = '''
CREATE TABLE all_filtered
AS
    WITH
        t1 AS 
        (
            SELECT 'nodes'::text AS src, nodes.tags, nodes.geom, nodes.bbox, nodes.centroid
            FROM nodes
            WHERE ''' + nodes_query_str + '''
        ), 
        t2 AS 
        (
            SELECT 'ways'::text AS src, ways.tags, ways.geom, ways.bbox, ways.centroid
            FROM ways
            WHERE ''' + ways_query_str + '''
        ), 
        t3 AS 
        (
            SELECT 'rels'::text AS src, relations.tags, relations.geom, relations.bbox, relations.centroid
            FROM relations
            WHERE ''' + rels_query_str + '''
        )

    SELECT t1.src, t1.tags, t1.geom, t1.bbox, t1.centroid FROM t1
    UNION
    SELECT t2.src, t2.tags, t2.geom, t2.bbox, t2.centroid FROM t2
    UNION
    SELECT t3.src, t3.tags, t3.geom, t3.bbox, t3.centroid FROM t3;
ALTER TABLE public.all_filtered OWNER TO myuser;

CREATE INDEX idx_all_filtered_geom
    ON public.all_filtered USING gist (geom)
    TABLESPACE pg_default;

CREATE INDEX idx_all_filtered_bbox
    ON public.all_filtered USING gist (bbox)
    TABLESPACE pg_default;

CREATE INDEX idx_all_filtered_centroid
    ON public.all_filtered USING gist (centroid)
    TABLESPACE pg_default;

CREATE INDEX idx_all_filtered_tags
    ON public.all_filtered USING btree (tags)
    TABLESPACE pg_default;'''

        with open(file_name, 'w', encoding='utf-8') as out_file:
            out_file.write(subqueries)

if __name__ == '__main__':
    gen = Generator()
    gen.generate_sql()
