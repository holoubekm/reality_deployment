from json import load as json_load

class Generator(object):
    def __init__(self):
        with open('./sql/generate_categories_config.json') as inp_file:
            self.config = json_load(inp_file)

    def generate_sql(self, file_name='./sql/add_categories.sql', table_name='all_filtered'):
        group_queries = []
        for group in self.config['groups']:
            group_name = group['name']
            column_name = f'{group_name}'
            queries = group['values']
            query_list = []
            for query in queries:
                condition_list = []
                for condition in query:
                    tag_name, tag_val = condition[0], condition[1]
                    if tag_val:
                        condition_list.append(f"tags->'{tag_name}'='{tag_val}'")
                    else:
                        condition_list.append(f"exist(tags, '{tag_name}')")
                condition_str = ' AND '.join(condition_list)
                condition_str = f'({condition_str})'
                query_list.append(condition_str)
            query_str = ' OR '.join(query_list)
            query_str = f"UPDATE {table_name} SET {column_name} = 1 WHERE ({query_str});"
            group_queries.append(f"ALTER TABLE {table_name} ADD COLUMN {column_name} INTEGER NOT NULL DEFAULT(0);")
            group_queries.append(query_str)
        subqueries = '\n\t'.join(group_queries)
        with open(file_name, 'w', encoding='utf-8') as out_file:
            out_file.write(subqueries)

if __name__ == '__main__':
    gen = Generator()
    gen.generate_sql()
