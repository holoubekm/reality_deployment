--ALTER TABLE public.ways
--    ADD COLUMN bbox geometry;

ALTER TABLE public.nodes ADD COLUMN centroid geometry;
ALTER TABLE public.ways ADD COLUMN centroid geometry;
ALTER TABLE public.relations ADD COLUMN centroid geometry;

ALTER TABLE public.ways RENAME COLUMN linestring TO geom;
ALTER TABLE public.relations ADD COLUMN geom geometry;

ALTER TABLE public.nodes ADD COLUMN bbox geometry;
ALTER TABLE public.relations ADD COLUMN bbox geometry;


