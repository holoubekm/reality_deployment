import psycopg2
import pandas as pd
import pandas.io.sql as sqlio

con = psycopg2.connect("dbname='pgsnapshot' user='myuser' host='127.0.0.1' port='5432' password='mypass'")
query = '''
            SELECT 
                MIN(ST_X(f.centroid)) AS min_lon, 
                MAX(ST_X(f.centroid)) AS max_lon,
                MIN(ST_Y(f.centroid)) AS min_lat, 
                MAX(ST_Y(f.centroid)) AS max_lat
            FROM all_filtered f
        '''

df = sqlio.read_sql_query(query, con)

min_lon = df['min_lon'].iloc[0]
max_lon = df['max_lon'].iloc[0]
min_lat = df['min_lat'].iloc[0]
max_lat = df['max_lat'].iloc[0]

num_tiles = 50

lon_span = max_lon - min_lon
lat_span = max_lat - min_lat
lon_delta = lon_span / num_tiles
lat_delta = lat_span / num_tiles

cur = con.cursor()
query = '''
        CREATE SEQUENCE IF NOT EXISTS tile_id_seq;
        CREATE TABLE IF NOT EXISTS tile (
            id INTEGER NOT NULL DEFAULT nextval('tile_id_seq'),
            geom geometry,
            PRIMARY KEY (id)
        )
        WITH (
            OIDS = FALSE
        )
        TABLESPACE pg_default;
        ALTER SEQUENCE tile_id_seq OWNED BY tile.id;


        ALTER TABLE public.tile
            OWNER to myuser;

        CREATE INDEX IF NOT EXISTS idx_tile_geom
            ON public.tile USING gist
            (geom)
            TABLESPACE pg_default;

        ALTER TABLE all_filtered ADD COLUMN tile_id INTEGER;
        ALTER TABLE all_filtered ADD CONSTRAINT all_filtered_tile_fk FOREIGN KEY (tile_id) REFERENCES tile (id);
        '''
cur.execute(query)
con.commit()

for x in range(num_tiles):
    for y in range(num_tiles):
        left = min_lon + lon_delta * x
        right = left + lon_delta
        bottom = min_lat + lat_delta * y
        top = bottom + lat_delta
        query = f'''INSERT INTO tile (geom) VALUES(ST_Polygon(ST_GeomFromText('LINESTRING({left} {bottom}, {right} {bottom}, {right} {top}, {left} {top}, {left} {bottom})'), 4326));'''
        cur.execute(query)
con.commit()


query = '''UPDATE all_filtered f SET tile_id = tile.id FROM tile WHERE ST_Contains(tile.geom, f.centroid);'''
cur.execute(query)
con.commit()

query = '''DELETE FROM tile t WHERE NOT EXISTS (SELECT * FROM all_filtered f WHERE f.tile_id = t.id)'''
cur.execute(query)
con.commit()

con.close()
