UPDATE nodes SET centroid = geom;
UPDATE nodes SET bbox = geom;

DO
$do$
BEGIN
    FOR i IN 1..5 LOOP
        WITH 
            rels AS (SELECT r.geom, r.id FROM relations r),
            mems AS (SELECT m.member_id, m.relation_id, m.member_type FROM relation_members m JOIN rels r ON m.relation_id = r.id),
            nds AS (SELECT n.id, n.geom, m.relation_id FROM nodes n JOIN mems m ON n.id = m.member_id WHERE m.member_type = 'N'),
            way AS (SELECT n.id, n.geom, m.relation_id FROM ways n JOIN mems m ON n.id = m.member_id WHERE m.member_type = 'W'),
            rrel AS (SELECT m.relation_id, r.geom FROM relations r JOIN mems m ON r.id = m.member_id WHERE m.member_type = 'R' AND r.geom IS NOT NULL),
            unioned AS 
            (
	            SELECT n.relation_id, n.geom FROM nds n
	            UNION
	            SELECT w.relation_id, w.geom FROM way w
	            UNION
	            SELECT l.relation_id, l.geom FROM rrel l
            )
            
            UPDATE relations r SET geom = b.geom 
            FROM (
		            SELECT u.relation_id, ST_Collect(u.geom) AS geom FROM unioned u GROUP BY u.relation_id
	             ) b
            WHERE r.id = b.relation_id AND r.geom is NULL;
    END LOOP;
END;
$do$;

UPDATE ways SET centroid = ST_Centroid(geom);
UPDATE relations SET centroid = ST_Centroid(geom);
UPDATE relations SET bbox = ST_Envelope(geom);

