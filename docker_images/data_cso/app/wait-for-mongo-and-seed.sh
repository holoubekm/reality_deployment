#!/bin/sh

MONGO_HOST=127.0.0.1
MONGO_PORT=27017

until nc -z $MONGO_HOST $MONGO_PORT
do
    echo "Waiting for MongoDB (127.0.0.1:27017) to start..."
    sleep 0.5
done

echo "Sleeping for 5s, just to be sure."
sleep 5



echo "********************************************"
echo "    Importing the Czech statistical data    "
echo "********************************************"
python3.7 ./seed_mongodb.py || exit 1
