import unicodedata
import re
import pandas as pd
from datetime import datetime
from pymongo import MongoClient
from os import path
from json import load as json_load


def date_time_formatted():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")

def normalize_rws(text):
    text = text.lower()
    text = unicodedata.normalize('NFKD', text).encode('ASCII', 'ignore').decode('ASCII')
    return re.sub(r'[^\w]+', '_', text)

def load_config(file_name, config_dir='configs'):
    config_path = path.join(config_dir, file_name)
    with open(config_path, 'r', encoding='utf-8') as inp_file:
        config = json_load(inp_file)
        return config

def create_mongo_collection_if_not_exists(database, collection_name):
    if collection_name not in database.list_collection_names():
        database.create_collection(collection_name, storageEngine={'wiredTiger': {'configString': 'block_compressor=zlib'}})
        return True
    return False

class CsoScraper(object):
    def __init__(self):
        super().__init__()
        self.config = load_config('cso_scraper_config.json')

        self.counties = {county.lower() for county in self.config['counties']}
        self.hash_cache = {}
        self.collection_name = f'czech_statistical_office_2018'

    @staticmethod
    def apply_modifier(df, modifier):
        if modifier == 'identity':
            return df
        else:
            raise Exception(f'Unimplemented modifier: [{modifier}]')

    @staticmethod
    def identity(df):
        return df

    @staticmethod
    def fix_corner_cases(county):
        if county == 'hlavní město praha':
            return 'praha-západ'
        return county

    @staticmethod
    def normalize_county_name(county):
        county = str(county).lower().replace('okres', '').strip()
        return CsoScraper.fix_corner_cases(county)

    def get_column_hash(self, col, name):
        col = normalize_rws(col)
        if col not in self.hash_cache:
            self.hash_cache[col] = {}

        if name not in self.hash_cache[col]:
            self.hash_cache[col][name] = f'cso_{col}_ord_float'
        return self.hash_cache[col][name]

    def scrape(self):
        client = MongoClient('mongodb://127.0.0.1:27017/')
        database = client["scraped"]

        create_mongo_collection_if_not_exists(database, self.collection_name)

        collection = database[self.collection_name]
        added_columns = set()
        print(f'Saving raw data to [{self.collection_name}]')

        configs = self.config['configs']
        written_count = 0
        for i, config in enumerate(configs):
            name = config["name"]
            file_name = config['file_name']
            keywords = config['keywords']
            cols = config['cols']
            not_added = set(self.counties)

            print(f'Parsing file: [{(i+1):03d} / {len(configs):03d} / {file_name}]')

            df = pd.read_excel(file_name, **keywords)
            df = CsoScraper.apply_modifier(df, config['modifier'])

            data = {}

            j = 0
            for row in df.itertuples():
                county = CsoScraper.normalize_county_name(row.Index)
                if county in not_added:
                    data[county] = {}
                    not_added.remove(county)

                    for col_idx, col in enumerate(cols):
                        hash_col = self.get_column_hash(col, name)

                        if j == 0:
                            assert hash_col not in added_columns, f'Duplicate column found in all the previous: [{hash_col}]'
                            added_columns.add(hash_col)

                        val = row[col_idx + 1]

                        if pd.isnull(val) or val is pd.np.nan:
                            print(f'np.nan or None value found: [{file_name}] [{col}]')
                            exit()
                        data[county][hash_col] = val
                j += 1

            assert len(not_added) <= 0, f'The file: [{file_name}] is missing some columns: [{not_added}]'
            item = {
                'inserted_at': date_time_formatted(),
                "name": name,
                "description": config["description"],
                "file_name": config["file_name"],
                "data": data
            }
            is_duplicate = {"name": name}
            written = collection.update_one(is_duplicate, {"$setOnInsert": item}, upsert=True)
            if written.upserted_id:
                written_count += 1
        print(f'Written: [{written_count:03d}] / [{len(configs):03d}] in total')


if __name__ == '__main__':
    scraper = CsoScraper()
    scraper.scrape()
