﻿$DesktopPath = [Environment]::GetFolderPath("Desktop")
$DockerDir = [System.IO.Path]::Combine($DesktopPath, "images")
Set-Location -Path $DockerDir

Start-Process "powershell" -ArgumentList "docker-compose up -d reality_mongodb"
Start-Sleep -s 2
Start-Process "powershell" -ArgumentList "docker-compose up -d reality_postgis"
Start-Sleep -s 2
Start-Process "powershell" -ArgumentList "docker-compose up -d reality_nominatim"
Start-Sleep -s 2
Start-Process "powershell" -ArgumentList "docker-compose up reality_api"
Start-Sleep -s 5
Start-Process "powershell" -ArgumentList "docker-compose up reality_web"
Start-Sleep -s 5
Start-Process firefox.exe -ArgumentList "-url http://127.0.0.1:5001"

$SlidesDir = [System.IO.Path]::Combine($DesktopPath, "prezentace")
Set-Location -Path $SlidesDir
$SlidesFile = [System.IO.Path]::Combine($SlidesDir, "Product demo - CZ - November 2018 - v003.pptx")
Start-Process firefox.exe -ArgumentList "-url $SlidesFile"
